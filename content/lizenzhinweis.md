+++
title = "Lizenzhinweis"
+++
Mit Beschluss der Mitgliederversammlung vom 24. Februar 2019,<br>
stehen alle vom Verein erstellten Inhalte, die auf dieser Internetseite verbreitet werden, unter der <a rel="license" href="https://creativecommons.org/licenses/by/4.0/deed.de">Creative Commons Namensnennung 4.0 International Lizenz</a>. <br>Dies dient der besseren Verbreitung und schafft Sicherheit für Nutzer eben dieser. <br><br>

**Ausgenommen:**<br>
Die Verwendung des Logos und zugehörige Grafiken, außerhalb von uns erstellten Inhalten.

Beispiel: Die Präsentation "DSGVO für Profis" beinhaltet unser Logo und zugehörige Grafiken. Wenn Du diese weiterveränderst oder verteilst, stellt das kein Problem dar.
Solltest Du jedoch das Logo aus der Präsentation kopieren und in einer neu erstellten verwenden, ist dies derzeit nicht erlaubt. Für diesen Fall wird ein entsprechendes Pressekit, unter eigener Lizenz, zur Verfügung gestellt.
<br><br>
**Noch Fragen?**
<br>
Schreibt uns doch eine <a href="mailto:postfach@anoxinon.de">E-mail</a>.

<br><br>
