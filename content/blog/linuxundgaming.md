+++
title = "Gaming und Linux?"
date = "2019-04-17T16:30:00+01:00"
tags = ["Anfänger", "Fortgeschritten"]
categories = ["Linux"]
banner = "/img/thumbnail/gamingundlinux.png"
description = "Das Thema Gaming war lange Zeit von Windows dominiert. In diesem Beitrag möchten wir Alternativen aufzeigen und nützliche Tipps geben."
+++

### I. Einführung
<img src="/img/thumbnail/gamingundlinux.png" class="fullsize"></img>
Ist es überhaupt möglich am PC zu spielen, ohne dabei Windows als
Betriebssystem zu nutzen? Diese Frage war früher durchaus berechtigt.
Die Antwort darauf lautet, heutzutage, eindeutig JA!
<br><br>
Aber warum ist dann Linux nicht weiter verbreitet, beispielsweise in der
Steam Statistik? Nun, das ist der typische Teufelskreis. Lange wurde
Gaming unter Linux recht stiefmütterlich behandelt und die
Spielherausgeber hatten keinen Grund gesehen für andere Plattformen zu
entwickeln, denn dies kostet zusätzlich Geld und Ressourcen. Im Zuge dessen
haben die Nutzer zum Spielen Windows nutzen müssen. Dies
hat sich in den letzten Jahren, zum Glück, sehr positiv verändert. Immer
mehr Spielproduzenten unterstützen jetzt, neben Windows, auch Linux
und MacOS. Hinzu kommt die immense Unterstützung der größten
Spiele-Verkaufsplattform im Netz: Steam. Unabhängig davon, welche
Meinung man von Valve und dessen Mitbegründer Gabe Newell haben mag, eines lässt sich nicht abstreiten:
Steam hat Spielen unter Linux ganz massiv nach vorne gebracht.
<br><br>
Im folgenden Artikel gehen wir auf Online Shops, Tools sowie
Möglichkeiten ein, wie sich die Nutzung von Windows gänzlich vermeiden
oder zumindest einschränken lässt.
<br>

### II. Einkaufen

Bevor wir richtig los legen können, müssen die Spiele zuerst einmal gekauft
sein. Mittlerweile gibt es recht viele Online Shops, nachfolgend ein Überblick der
vier bekanntesten.
<br><br>

-   [**Steam**](https://store.steampowered.com/) ist die größte
    Online-Plattform im Spielebereich. Die Auswahl ist riesig und kaum
    ein Spielentwickler kommt an diesem Marktplatz vorbei. Pro
    verkauftem Spiel erhält Steam etwa 20 bis 25 % des Erlöses. Eine
    Rückerstattung ist innerhalb von 14 Tagen möglich. Voraussetzung ist
    hierfür eine Spielzeit unter zwei Stunden, für den jeweiligen Titel.
    Die [**Steam Software**](https://store.steampowered.com/about/) ist
    für Linux, Windows sowie macOS, erhältlich. Optional kann auch das
    eigens für Linux entwickelte
    [**SteamOS**](https://store.steampowered.com/steamos/) genutzt
    werden.

-   [**itch.io**](https://itch.io/) ist ein Marktplatz speziell für
    Entwickler. Dieser hebt sich dadurch hervor, dass nicht nur der
    Produktpreis frei bestimmt werden kann, sondern auch wie viel
    Prozent des Erlöses an itch.io gehen soll. Besonders für
    Programmierer von Indie-Games (unabhängige Entwickler) ist diese
    Plattform besonders wertvoll. Die Rückerstattung wird individuell
    gehandhabt und kann hier nachgelesen werden:
    [**Refunds**](https://itch.io/docs/legal/terms#9-refunds). Die
    [**itch.io App**](https://itch.io/app) ist ebenfalls für Linux,
    Windows und macOS verfügbar.

-   [**GOG**](https://www.gog.com/) zeichnet sich vor allem durch viele
    Spielklassiker, sowie den Verzicht auf
    [**DRM**](https://de.wikipedia.org/wiki/Digitale_Rechteverwaltung)
    (keine Einschränkungen nach dem Erwerb) aus. Auch aktuelle
    Spiele können dort gekauft werden, sofern die Spielherausgeber auf
    DRM verzichten. GOG erhält 30 % pro verkauftem Spiel als Erlös. Bei technischen Problemen besteht ein 30-tägiges Rückgaberecht mit
    voller Erstattung, sofern innerhalb dieses Zeitraumes keine Lösung
    durch den Kundendienst gefunden wurde. Die passende Software
    [**GOG-Galaxy**](https://www.gog.com/galaxy) ist leider noch immer
    nicht für Linux verfügbar, sondern nur für Windows und macOS. Die
    Spiele selbst können jedoch auch ohne den Galaxy Client herunter
    geladen und installiert werden.

-   [**Humble Bundle**](https://www.humblebundle.com/) hat ein
    außergewöhnliches Konzept und möchte mit dem Verkauf soziale
    Projekte und Hilfsorganisationen unterstützen. Spiele werden zumeist
    in Paketen angeboten, beginnend ab einem Dollar. Der Wert an
    erworbenen Spielen, im Vergleich zum bezahlten Preis, ist stets um
    einiges höher. Ab einem festgelegten, steigenden Betrag erhält der
    Käufer dann mehr und mehr Spiele für seine Sammlung. Alternativ kann
    ein Abonnement abgeschlossen werden und man erhält jeden Monat,
    nochmals vergünstigt, Zugriff auf die wechselnden Pakete. Zudem gibt
    es wöchentliche Angebote, welche oftmals sehr interessante
    Spiele zum kleinen Preis enthalten. Der Käufer hat ferner noch die
    Möglichkeit, seinen Kaufbetrag unterschiedlich zu verteilen. Wie
    hoch der Anteil für Spielhersteller, soziale Projekte oder Humble
    Bundle selbst ausfällt, entscheiden die Käufer. Die Voreinstellung
    sieht vor, dass der Kaufbetrag zu gleichen Teilen aufgeteilt wird.
    Es kann aber auch ausgewählt werden, dass der komplette Erlös
    beispielsweise an eine soziale Organisation gespendet wird.

    Eine Rückerstattung ist bis zu 60 Tagen möglich und wird einzeln
    durch den Kundendienst bearbeitet. Für Humble bundle ist keine
    Software erhältlich. Der Käufer erhält zumeist den Produktschlüssel
    für ein Spiel und kann diesen im Steam einlösen und den Spieltitel
    direkt dort installieren. Eine eigene Software gibt es nicht, was
    wiederum aber auch nicht notwendig ist.

<br>
Unbedingt anzumerken sei noch, dass ein datenschutzfreundlicher Einkauf
an sich, nur mit Geschenkkarten durch Barzahlung möglich ist. Online
Bezahldienste von Drittanbietern, sind aus Sicht der Datenübermittlung,
zu meiden. Auch die [**Paysafecard**](https://www.paysafe.com/de/) ist
keine Alternative, da hier das
[**Tor-Netzwerk**](https://www.torproject.org/) aktiv blockiert wird.
Außer Steam bietet leider keiner der oben genannten Shops bislang
Geschenkkarten an.


### III. Loslegen und Spielen
Nun kommen wir zum wichtigsten Teil, dem Spielen selbst.Drei
Möglichkeiten kommen am Computer dazu in Frage: Ausschließlich Linux als
Betriebssystem, Linux und Windows als separate Systeme nebeneinander <b><a href="https://wiki.ubuntuusers.de/Dualboot/">Dual Boot</a></b>, oder Windows
als virtualisierte Maschine.
<br>
<br>
<img style="float:left;margin-right: 10px; margin-bottom: 10px;"src="/img/gaming/anxi-tool.png"; width="60"></img>
#### Windows in einer virtualisieren Umgebung
<br>
Virtualisierung bedeutet, dass wir ein Betriebssystem virtuell neben
unserem Hauptbetriebssystem starten.

Damit ist es möglich, Windows als „zweites OS" zu starten. Jedoch bietet
dies, selbst mit entsprechenden Anpassungen, keinen kompletten
Privatsphäre-Gewinn (Telemetrie: <b>
<a href="https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Cyber-Sicherheit/SiSyPHus/Analyse_Telemetriekomponente.pdf?__blob=publicationFile&v=3">BSI Analyse als
PDF</a></b>).
Wer jedoch unbedingt alle Spiele lauffähig haben möchte, ist mit dieser
Lösung am besten beraten. So bleibt zumindest das Produktivsystem mit
den wirklich wichtigen und sensibleren Daten abgeschottet. Der
Performance Verlust ist minimal und in jedem Fall verschmerzbar.
<br>
<br>
<b>Voraussetzungen:</b>
<br><br>
 1. Ein nicht all zu alter Prozessor (CPU) mit integrierter
    Grafikeinheit (nicht alle CPUs besitzen eine). Alternativ werden
    zwei Grafikkarten benötigt, wobei nur eine gute Leistungswerte haben
    muss.
<br><br>
2.  <b><a href="https://de.wikipedia.org/wiki/VT-d">Virtualisierung</a></b> muss
    sowohl vom Prozessor als auch dem Mainboard unterstützt werden. Bei
    AMD CPUs muss AMD-V und bei Intel VT-D vorhanden sein. Um zu testen,
    ob der Prozessor die nötige Voraussetzung hat, gebt ihr folgenden
    Befehl in der Linux-Konsole ein: <code>lscpu</code>
<br><br>
Die Ausgabe sieht dann wie folgt aus (abhängig vom Prozessorhersteller):
<img src="/img/gaming/amd-v.png" class="fullsize"></img>
In diesen Beispielen handelt es sich einmal um einen AMD Ryzen 7 1700X
Prozessor, welcher AMD-V und somit Virtualisierung unterstützt.
<img src="/img/gaming/intel_vtx.png" class="fullsize"></img>
Wie auf diesem Bild zu sehen ist, unterstützt der Intel i7-4770K
Prozessor kein VT-D (VT-X reicht nicht aus) und ist somit nicht geeignet
für diesen Einsatzzweck.<br><br>
<img style="float: left;margin-right: 10px; margin-bottom: 10px;" src="/img/gaming/info.png" width="40"></img>
<b>Sollten beide Voraussetzungen (zwei Grafikeinheiten und AMD-V bzw. Intel VT-D) nicht gegeben sein, <br>
so kommt Windows, als virtuelle Gaming Maschine, nicht in Frage!</b>
<br><br>
Für eine ausführliche Anleitung besucht bitte die Seite <b><a href="https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF">Arch Wiki:
PCI\_Passthrough</a></b>.
Es gibt zudem viele Anleitungen und Infos dazu im Netz. Als Suchbegriff
könnt ihr „gpu passthrough" in die Suchmaschine eingeben und kommt so
auf weitere Seiten mit Anleitungen und Hilfestellungen.
<br><br>
<img src="/img/gaming/anxi-tool.png"; style="float:left;margin-right: 10px; margin-bottom: 10px;" width="60"></img>
#### Spielen nur mit Linux
<br>
Viele Spiele laufen bereits nativ unter Linux. Für einige andere Titel
gibt es diverse Hilfsmittel, am bekanntesten ist
<b><a href="https://www.winehq.org/">WINE</a></b>.

Was ist WINE? Die Homepage selbst fasst es zusammen:

>Wine (ursprünglich ein Akronym für "Wine Is Not an Emulator") ist eine
Kompatibilitätsschicht, die es ermöglicht, Windows-Anwendungen unter
POSIX-konformen Betriebssystemen auszuführen, wie z.B. Linux, macOS und
BSD. Statt interne Windows-Logik zu simulieren, wie eine virtuelle
Maschine oder ein Emulator, übersetzt Wine die Windows-API-Aufrufe in
Echtzeit zuentsprechenden POSIX- Aufrufen und eliminiert somit die
Performance- und Speichereinbußen, die andere Methoden nach sich ziehen.
Wine erlaubt es auf diese Weise, Windows-Anwendungen sauber in Ihre
Desktopum- gebung zu integrieren.


Auf der Codebasis von WINE hat Steam 2018 die eigene, integrierte
Software [**Proton**](https://github.com/ValveSoftware/Proton)
entwickelt und unter dem Namen „Steam Play" veröffentlicht. Damit
kümmert sich Steam nun eigenhändig um die Installation und Anpassung der
Spieletitel. Die Liste der offiziell unterstützten Spiele steigt
kontinuierlich an und kann hier eingesehen werden: [**Proton kompatible
Spiele**](https://store.steampowered.com/curator/33483305-Proton-Compatible/#browse).

Offiziell bedeutet, die Spiele wurden ausgiebig getestet. Noch ist die
Liste nicht all zu lang, was aber nicht bedeutet, dass andere
Titel nicht unter Linux lauffähig sind. Eine hervorragende Übersicht
findet ihr auf [**ProtonDB**](https://www.protondb.com/).
<img src="/img/gaming/protondb.png" class="fullsize"></img>
Hier könnt ihr nach Spielen suchen. Der jeweilige Status zur Linux Kompatibilität wird in Kategorien angezeigt. Von „borked" (nicht spielbar) bis hin zu Platinum (perfekt spielbar) wird übersichtlich dargestellt, ob und wie gut das entsprechende Spiel von Proton in Steam Play zum Laufen gebracht werden kann.
<br>
<br>
<img style="float: left;margin-right: 10px; margin-bottom: 10px;" src="/img/gaming/lutris.png" width="60"></img>
#### Lutris - Spieleverwaltung für Linux

<img src="/img/gaming/lutris_hp.png" class="fullsize"/></img>
Mit [**Lutris**](https://lutris.net) könnt ihr Spiele hinzufügen, verwalten und verschiedenste
Optionen anpassen. In der aktuellen 0.5 Version können nun auch Spiele
aus eurer GOG Bibliothek direkt installiert werden. Des Weiteren sind
passende Installations-Skripte nun in die Software integriert, dazu
gleich mehr.

Bereits vorhandene Spiele auf der Festplatte können über die
Importfunktion direkt zu Lutris hinzugefügt werden. Das Herzstück der
Software ist jedoch die Möglichkeit, Spiele über die Bereitstellung von
Skripten, installieren zu können. Einige Titel, die beispielsweise nicht
direkt von Steam unterstützt werden oder Probleme verursachen, können
direkt mit den entsprechenden Optimierungen automatisiert installiert
werden. Hierfür werden Installations-Skripte auf
[**lutris.net**](https://lutris.net/) kontinuierlich angepasst und der
Community zur Verfügung gestellt.

Lutris bietet sehr viele Anpassungsmöglichkeiten und ist wahrlich eine
große Bereicherung für alle Linux Gamer!
<br>

Als Alternative zu Lutris möchten wir noch
[**Gamehub**](https://tkashkin.tk/projects/gamehub/) erwähnen. Die
Oberfläche selbst ist ein wenig schöner, jedoch ist der Funktionsumfang
noch deutlich geringer als bei Lutris. Es lohnt sich aber auf jeden Fall
beide Projekte im Auge zu behalten!

### IV.Tracking

Leider ist das Ausspähen und Verfolgen der Nutzeraktivitäten auch im
Gaming Bereich weit verbreitet. Zum Glück gibt es eine tolle Seite, die
sich genau dieser Thematik annimmt und versucht gegen zu steuern:
<b><a href="https://gameindustry.de">GameIndustry</a></b>.
<img src="/img/gaming/gameindustry.png" class="fullsize"/>

Die erschreckend lange Liste (Hosts) lässt sich direkt im System oder
zentral über eine Firewall einbinden. Detaillierte Informationen
erhaltet ihr direkt auf der Seite selbst oder unter
<b><a href="https://notabug.org/angrytux/gaming_without_spying">Gaming without
spying</a></b>

### V. Empfehlenswerte Seiten

1. <b><a href="https://gamingonlinux.com">Gamingonlinux</a></b> ist eine tolle Anlaufstelle für Linux Gaming. Neben einem Forum gibt es Spieletests, eigene Gameserver, aktuelle Spielevorstellungen und vieles mehr.
Besonders schön ist die Seite für <b><a href="https://www.gamingonlinux.com/free-games/">kostenlose Spiele</a></b>, mit der auch explizit nach freien,  quelloffenen Spielen gesucht werden kann.
<img src="/img/gaming/gamingonlinux.png" class="fullsize"/>
 <a href="https://mastodon.social/@gamingonlinux"><img src="/img/gaming/mastodon.png" title="Gamingonlinux auf Mastodon" width="40"/></a> Gamingonlinux ist auch im Fediverse erreichbar!
<br>
<br>
2. <b><a href="https://www.holarse-linuxgaming.de/">Holarse</a></b> ist ebenfalls eine hervorragende Seite für den deutschsprachigen Raum. Eigene Spieleserver, Artikel zu Tools und Spielen und vieles mehr, sind hier zu finden. Eine Übersicht für Open Source Games darf hier natürlich auch nicht fehlen:<b> <a href="https://www.holarse-linuxgaming.de/tags/200%2C201">Spiele, Open Source</a></b>.
Besonders toll für gemeinsames Spielvergnügen ist der <b><a href="https://www.holarse-linuxgaming.de/wiki/holarse_spieleabend">Holarse Spieleabend</a>!</b>
<img src="/img/gaming/holarse.png" class="fullsize"/>
<a href="https://mastodon.social/@holarse"><img src="/img/gaming/mastodon.png" title="Holarse auf Mastodon" width="40"/></a> Holarse ist auch im Fediverse erreichbar!

<img style="float: left; margin-right: 10px; margin-bottom: 10px;"src="/img/gaming/warriors.png" width="100">
### VI. Free Open Source Game
Nachfolgend eine kleine Auswahl von Spielen, deren Quellcode komplett
offen gelegt ist:
<br><br>

- [**The Battle for Wesnoth:**](https://www.wesnoth.org/) Anspruchsvolles, rundenbasiertes Strategiespiel. Inzwischen auch
       in Steam verfügbar.

- [**Xonotic:**](https://www.xonotic.org/)
3d Shooter im Stil des legendären Unreal Tournament.

- [**SuperTuxKart:**](https://supertuxkart.net/Main_Page)
Die Alternative schlechthin für alle MarioKart Fans!

- [**OpenRA:**](https://www.openra.net/)
Command & Conquer RTS neu aufgelegt als open source Variante!

- [**The Dark Mod:**](http://www.thedarkmod.com/main/)
Für alle Liebhaber von Thief: The Dark Project - Schleichen im Dunkeln.

- [**Argentum Age:**](http://argentumage.com/)
Taktisches Sammelkartenspiel mit sehr viel Finesse.

<br>
Die Liste könnte ewig so fortgeführt werden. Es gibt sehr viele, richtig
gute open source Spiele!
Neben den oben empfohlenen Seiten liefern die
folgenden Links eine Übersicht:
<br>
<br>
[**https://wiki.archlinux.org/index.php/List\_of\_games**](https://wiki.archlinux.org/index.php/List_of_games)

[**https://de.m.wikipedia.org/wiki/Liste\_quelloffener\_Computerspiele**](https://de.m.wikipedia.org/wiki/Liste_quelloffener_Computerspiele)


### VII. Alternativen zum Computer
<img style="float: left; margin-right: 10px; margin-bottom: 10px;" src="/img/gaming/gameboy.png" width="30"><br><b>Mobile Gaming</b>
<br><br>
Smartphones sind heutzutage kaum noch aus dem Alltag wegzudenken. Daher
verwundert es nicht, dass die mobile Plattform den größten Anteil am
Spielemarkt ausmacht.<sup>1</sup>

Der Fokus dürfte hier allerdings zumeist bei Gelegenheitsspielen für
Zwischendurch liegen. Trotz stetig performanter werdenden Smartphones,
erreichen diese längst noch nicht die grafischen Möglichkeiten eines
aktuellen Desktopcomputers. Auf die Trackingmöglichkeiten und
Problematiken, sei an dieser Stelle nur kurz hingewiesen. Eine genau
Analyse würde den Rahmen für diesen Artikel mehr als sprengen.
<br><br>
<img style="float: left; margin-right: 10px; margin-bottom: 10px;" src="/img/gaming/gamepad.png" width="50"><b>Spielekonsolen</b>
<br><br>
Sie sind einfach zu bedienen und der Käufer muss sich seltenst Gedanken
um die richtige Konfiguration machen. Anschließen und loslegen lautet
hier die Devise.

Es gilt jedoch zu beachten, dass man sich immer noch eine Blackbox an
den Fernseher anschließt und man den Datenaustausch nicht kontrollieren
kann. Die größten Konsolenhersteller veröffentlichen keinen Quellcode.
Dennoch ist bisher kein Fall bekannt, dass Konsolen den Nutzer 24 / 7
ausspähen. Es lohnt sich auch ein Blick auf das Portal „Privacy not
included" der Mozilla Foundation:

PS4:
[**https://foundation.mozilla.org/en/privacynotincluded/products/ps4/**](https://foundation.mozilla.org/en/privacynotincluded/products/ps4/)

XBOX One:
[**https://foundation.mozilla.org/en/privacynotincluded/products/xbox-one/**](https://foundation.mozilla.org/en/privacynotincluded/products/xbox-one/)

Nintendo Switch:
[**https://foundation.mozilla.org/en/privacynotincluded/products/nintendo-switch**](https://foundation.mozilla.org/en/privacynotincluded/products/nintendo-switch)
<br>
<img style="float: left; margin-right: 10px; margin-top: 10px;" src="/img/gaming/cloud.png" width="40"><br><b>Cloud Gaming</b>
<br><br>
Anstatt die Videospiele lokal auf seinem Rechner laufen zu lassen, nutzt
man starke Serverhardware. Die Vorteile sind offensichtlich, es braucht
keine starke Hardware, da alle Grafikberechnungen auf dem Server des
Anbieters durchgeführt werden. Somit entfallen eventuell hohe
Anschaffungskosten für Grafikkarte, Mainboard, Prozessor etc. Auch spart
man sich das Herunterladen der Spiele. Somit kann man von jedem Rechner
aus seine Bibliothek genießen, ob am Laptop, Desktop oder am Fernseher.

Es gilt zu beachten, dass zumeist recht hohe, monatliche Gebühren für
die Nutzung des Dienstes anfallen. Ferner hat man bei Ausfall des
Dienstes selbstverständlich keinerlei Zugriff auf seine Bibliothek.
Ebenfalls benötigt man eine adäquate Internetleitung und die Latenz
(Ping) spielt eine wichtige Rolle.

### VIII. Schlusswort

<img src="/img/gaming/anxi_ende.png" width="110" align="left" vspace="10" hspace="10" alt="Text?">Windows ist längst nicht mehr die einzige Möglichkeit, Videospiele am PC
zu genießen! Spätestens nach der Einführung von Steam Play ist ein sehr
großer Teil der Spiele auch unter Linux verfügbar geworden. Trotz all
der äußerst positiven Entwicklungen wollen wir aber nichts schön reden.
Linux Gaming ist auf einem sehr guten Weg, aber kann noch nicht alle
Anforderungen jedes Spielers erfüllen. In Verbindung mit einfach zu bedienenden Tools, wie dem vorgestellten
Lutris, sowie tollen Informationsseiten im Bereich Linux Gaming, macht
Spielen unter Linux noch mehr Spaß!
<br><br>
Solltet Ihr noch immer Bedenken gegenüber Linux haben, so legen wir euch
unsere Artikel zu diesem Thema ans Herz:
<br><br>
[**Linux für Einsteiger Teil**](https://anoxinon.media/blog/linuxfuereinsteiger1/) - gute Gründe
für Linux!

[**Linux für Einsteiger Teil 2**](https://anoxinon.media/blog/linuxfuereinsteiger2/) -- Vorbereitung und
Installation.
<br><br>
Viel Spaß beim Spielen!
<br><br>
<div style="text-align: center"><a href="/files/linuxgaming.pdf" class="btn btn-small btn-template-main">Download Artikel als PDF</a></div>

---

1 <https://www.go-globe.com/blog/mobile-gaming-industry/>

Titelbild (hier verändert) sowie Icons für Alternativen und FOSS Games
von <a href="https://openclipart.org/"><img src="/img/gaming/openclipart.png" title="Openclipart" width="150"/></a>

Lutris Makotchen von <https://lutris.net/>
