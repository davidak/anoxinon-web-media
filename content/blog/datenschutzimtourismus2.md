+++
title = "Datenschutz im Tourismus - Teil 2"
date = "2019-06-06T18:30:00+02:00"
tags = ["Anfänger"]
categories = ["Datenschutz"]
banner = "/img/thumbnail/Pass.png"
description = "Reise gebucht? Sind Sie gerade bei den Reisevorbereitungen? In dem zweiten Beitrag der Artikelserie möchten wir auf dubiose Empfehlungen eingehen und sinnvolle Alternativen vorstellen."

+++
> Trotz sorgfältiger Recherche behalten wir uns Irrtümer vor.

Nachdem wir im [ersten Beitrag](/blog/datenschutzimtourismus1/) bereits auf den Buchungsprozess und den dazugehörigen Abhängigkeiten eingegangen sind, möchten wir im zweiten Beitrag dubiose Empfehlungen näher beleuchten. Außerdem geben wir Tipps zur Reisevorbereitung und stellen Alternativen vor.

---

**Inhaltsverzeichnis:**

[I. Dubiose Empfehlungen](#I._Dubiose_Empfehlungen)  
[II. Richtige Reise Tipps](#II._Richtige_Reise_Tipps)  
[III. Open Source Apps für die Reise](#III._Open_Source_Apps_für_die_Reise)  
[IV. Flugzeug-WLAN / Hotel-WLAN - Verzichten Sie besser darauf!](#IV._Flugzeug-WLAN_/_Hotel-WLAN_-_Verzichten_Sie_besser_darauf!)  
[V. Visum](#V._Visum)  
[VI. Schlussgedanken](#VI._Schlussgedanken)  

---
### I. Dubiose Empfehlungen<a name="I._Dubiose_Empfehlungen"></a>

Im Zuge unserer Recherche, haben wir uns diverse Empfehlungen zur Reisevorbereitung durchgelesen. Im Detail sind uns die Tipps von "Reiseblogs" aufgefallen. Normalerweise finden sich dort Einträge von malerischen Sandstränden, idyllischen Wäldern und aufregenden Bergtouren, jedoch auch solche zur Reisevorbereitung.

Hier werden teilweise "Tipps und Tricks" beschrieben, die proprietärer Software, Datenkraken und Kriminellen Tür und Tor öffnen. Oft fallen noch dazu Wörter wie **"smart"** und **"sicher"**, obwohl viele Empfehlungen genau das Gegenteil bewirken.

Leider nehmen die Leser diese Tipps, oft unkritisch, auf und hinterfragen deren Inhalt nicht kritisch. Meist glauben sie, dass Reiseexperten auch in technischer Hinsicht gute Kenntnisse besitzen. Oft is dies jedoch nicht der Fall. Unter Umständen übernehmen die Leser also unreflektiert das Verhalten des Autors.

Beginnen wir mit dem ersten weitverbreiteten Tipp:

>"Unterwegs verliert man gerne mal seinen Krempel. Oder er wird einem geklaut. Kommt vor, kann man manchmal einfach nicht vermeiden. Umso smarter, wenn man vor Antritt der Reise Fotos von allen wichtigen Dokumenten gemacht und in seiner Dropbox, im Emailpostfach oder jedweder anderen Cloudlösung abgelegt hat. Übrigens: wer seine iPhone-Fotos regelmäßig in die Cloud hoch lädt (oder automatisch hochladen lässt) verliert sie nicht, wenn das iPhone geklaut wird. "
<small>[Tipps für sicheres und entspanntes Reisen, Parkvogel.de](https://www.parkvogel.de/blog/paulas-travel-hacks/)</small>

> "Stell dir vor: Du bist weit weg. Rundherum nur Sonne, Strand und Meer. Plötzlich sind deine wichtigsten Reisedokumente weg und die nächste Botschaft ist hunderte Kilometer entfernt. Bist du jetzt noch entspannt? Wohl nicht mehr. Es ist (überlebens)wichtig, dass du schon im Vorfeld deiner Reise daran denkst, deine wichtigsten Dokumente einzuscannen und in der Cloud abzuspeichern. Wir nutzen dafür Google Drive und zusätzlich Dropbox (doppelt hält besser)."
<small> [Life Hacks Reise, Ausreiser.com](https://www.ausreiser.com/lifehacks-reise/)</small>

Was kann also konkret passieren, wenn man diesen Tipp beherzigt?

Sie laden Ihre wichtigsten Dokumente, wie Personalausweis oder Reisepass, in eine US-Cloud. Dort gelten andere Datenschutzgesetze und Strukturen, als wir sie innerhalb der EU kennen. Die Anbieter leben meist vom Verkauf Ihrer Daten, was auch zahlreiche Skandale in der Vergangenheit bestätigt haben. Außerdem ist es möglich, dass Kriminelle Zugriff auf Ihre gespeicherten Dokumente erhalten - sei es durch ein unsicheres Endgerät, durch Abfangen der Verbindung in ungesicherten WIFI-Hotspots, oder durch Datenleaks.

Es kann also durchaus sein, dass Sie sich dann selbst im Urlaub gegenüberstehen - zumindest auf dem Papier. Dokumente wie Personalausweise oder Reisepässe sollten unter keinen Umständen im Internet landen. Aus gutem Grund müssen Sie zur Identifizierung in die deutsche Botschaft, wenn Sie Ihre Unterlagen im Ausland verloren haben. Da hilft auch keine Kopie, die Sie vorzeigen können. Sie können mit dieser weder fliegen noch wird eine Behörde gescannten oder kopierte Dokumenten einfach Glauben schenken.

Grundsätzlich ist es **nicht zu empfehlen** Reisepässe, Personalausweise o.ä. digital zu erfassen.

---

<img src="/img/thumbnail/Pass.png" class="fullsize" />

---

>"Laptops brauchen keine luxuriöse Laptoptasche, man kann sie auch prima in Umschlägen mit Luftpolsterfolie transportieren. [...] So ist euer wertvollstes Gut [...] nicht nur sicher verpackt, sondern auch diebstahlgeschützt - wer klaut schon einen ollen knittrigen Umschlag?! “
<small>[Paulas Travel Hacks, Parkvogel.de](https://www.parkvogel.de/blog/paulas-travel-hacks/)</small>

Ob eine Luftpolsterfolie ausreicht, um einen ausreichenden Diebstahlsschutz zu gewährleisten, lassen wir hier einmal offen. Wenn die Folie entpackt wird und man an sensible unverschlüsselte, Daten kommt (z.B. Pässe, Passwörter und Co.), war der Schutz nicht ausreichend. Auch hier gilt die Devise: Verschlüsseln Sie Ihre Festplatte, damit im Falle eines Diebstahls Ihre Daten nicht in die falschen Hände geraten.

>„Skype sagt dir ja sicher etwas – günstiger kannst du eigentlich nicht zuhause anrufen, um zu berichten, wie die Reise bisher war. Mein Tipp: Nutze die App Viber – das ist mein Favorit!“
<small>[Backpacking-Hacks: MacGyver für unterwegs, Travelicia.de]( https://www.travelicia.de/backpacking-hacks/)</small>

Die genannten Apps können nicht nur dabei helfen, die Liebsten günstig zu erreichen. Sie können auch „nachhause telefonieren" und private Angelegenheiten (inkl. Ihrer Kontakte), sind auf einmal nicht mehr ganz privat, sondern befinden sich sodann
auf den Servern der entsprechenden Unternehmen und Werbepartner. Mehr Informationen dazu erhalten Sie in [diesem Beitrag](/blog/diemachtderdatensammler/).

>Smart connected – egal wo: Ein Hotelzimmer ohne freies WLAN? Tabu! Oftmals lässt jedoch die Schnelligkeit der Verbindungen zu wünschen brig, einige Hotels beschränken den Zugang auch auf eine bestimmte Nutzerzahl. Zuverlässiger verbunden ist man mit einem eigenen portablen WLAN-Router, der auch am Flughafen oder unterwegs mit der Bahn für barrierefreies Arbeiten sorgt.
> Smart connected – egal wo: Ein Hotelzimmer ohne freies WLAN? Tabu! Oftmals lässt jedoch die Schnelligkeit der Verbindungen zu wünschen übrig, einige Hotels beschränken den Zugang auch auf eine bestimmte Nutzerzahl. Zuverlässiger verbunden ist man mit einem eigenen portablen WLAN-Router, der auch am Flughafen oder unterwegs mit der Bahn für barrierefreies Arbeiten sorgt.
<small>[Business Travel Hacks: 10 Tipps für eine reibungslose Geschäftsreise, smartworkers.de](https://www.smartworkers.net/2015/01/business-travel-hacks-10-tipps-fuer-eine-reibungslose-geschaeftsreise)</small>

WLAN ist grundsätzlich nicht sicher, gerade ein fremdes WLAN an öffentlichen Orten wie Flughäfen etc. ist ein Risiko, das seit Jahren für Gesprächsstoff sorgt. Wieder wurde das Zauberwort “smart” verwendet ohne Begründung, warum dieses Risiko smart sein soll. Zwar ist dadurch ein “barrierefreies” Arbeiten möglich, aber der Preis ist ein hohes Sicherheitsrisiko. Dazu zählt das Abfischen von Nutzerdaten oder das Ausspionieren des Browserverlaufs. Wenn Ihr Firmenlaptop befallen ist, kann dies auch leicht in Industriespionage ausarten.

VPN, also eine gesicherte Verbindung, ist unbedingt notwendig. Bei VPN ist es zudem wichtig, sich selbst um diesen Kanal und eine sichere Verbindung zu kümmern, da man leicht auf Werbeversprechen der Anbieter hereinfallen kann. Dafür gibt es keine “Zauberlösung”. Auch die negative Darstellung, dass die WLAN-Begrenzung im Hotel ein Tabu ist, ist wieder Zauberwort-Framing. Ist es nicht gerade gut, wenn Hotels den Zugang auf das WLAN beschränken, um einen Angriff darauf zu minimieren? Auch das Hotel-WLAN sollte mit Vorsicht genossen werden. Viele Hotelbetreiber ändern das WLAN-Passwort nicht, was dem WLAN die Sicherheit eines nahezu öffentlichen WLANs verleiht. Noch dazu kann der Betreiber Ihren gesamten Verlauf etc. analysieren und böswillige Dritte können Daten abfischen. Nutzen Sie daher unbedingt auch im Hotel einen VPN-Zugang.

---

<img src="/img/thumbnail/Passport.png" class="fullsize" />

---

Oft gibt es auch noch App Empfehlungen, wie Reiseplaner, Packhilfen für den Koffer sowie Checklisten. Hier stellt sich aber die Frage, warum vieles davon online sein muss und entsprechend viele Berechtigungen erfordert, von den Hintergrundverbindungen ganz abgesehen. Es ist wesentlich sinnvoller diese Inhalte lokal abzuspeichern.

Zu guter Letzt werden des öfteren ungeschwärzte Flugtickets online geteilt. Namen und Buchungsnummern sind dort abgebildet, man kann damit ganz einfach an die Buchungsdaten gelangen. Dafür benötigt man nur die Daten auf Ihrem Ticket. Manchmal reicht sogar nur die Buchungsnummer. Wenn Flugtickets unbedingt geteilt werden “müssen”, sollte mindestens ein Hinweis, vor allem für die jüngere Leserschaft dabei sein, dass Daten geschwärzt wurden, um Risiken zu vermeiden. So trägt man etwas zur Bewusstseinsbildung bei.

Es gab auch einige Blogautoren (inklusive der oben genannten), die wir darauf angesprochen haben. Entweder wurde überhaupt nicht reagiert, oder schon der Outlook/Gmail Server lehnte unsere E-Mail ab. Da keine weitere Kontaktmöglichkeit bestand, haben wir es nicht weiter versucht. Auch waren wir mit Buchautoren in Kontakt, die ähnliche Tipps in ihren Büchern verwendet haben und das ohne ausreichende Recherche. Hier gab es ebenso kaum Rückmeldungen, ausgenommen eine mit dem Hinweis, dass die Recherche 2015/2016 durchgeführt wurde und die Thematik damals nicht relevant war. Das möchten wir aber so nicht gelten lassen. Damals gab es zwar uns nicht, jedoch viele andere Seiten wie die [Digitalcourage](www.digitalcourage.de), [Kuketz Blog](www.kuketz-blog.de), das [Privacy Handbuch](www.privacy-handbuch.de), [Mobilsicher](www.mobilsicher.de) - auf denen man sich zu Genüge informieren konnte.

---

### II. Richtige Reise Tipps<a name="II._Richtige_Reise_Tipps"></a>

Wir haben, bevor wir diese Reihe gestartet haben, ausgiebig recherchiert und wollten Ihnen zeigen, worauf Sie achten sollten, um die größtmögliche Datensouveränität beim Reisen zu genießen.

*  Daten IMMER verschlüsseln! Am besten auch den Laptop, damit die Daten auch bei Verlust sicher sind. Dabei auch auf die Art der Verschlüsselung achten. Wir empfehlen Verschlüsselungstools wie LUKS oder [VeraCrypt](www.veracrypt.de), anstelle von Tools wie BitLocker. Bedenken Sie auch: Ein Word-Dokument oder eine Excelliste mit Passwort zu versehen ist weder verschlüsselt noch sicher! Bedenken Sie außerdem, dass manche Urlaubsländer bzw. die dortigen Behörden von Ihnen verlangen können, dass Sie Ihre Passwörter herausgeben. Auch Geschäftshandys sind hier kritisch, da Sie auf diese Weise sensible Geschäftsgeheimnisse preisgeben. Welche Länder das so handhaben, können Sie auf [Privacytools.io](privacytools.io) nachlesen. Schlimmer noch, einige Länder räumen sich sogar das Recht ein sämtliche Daten Ihres mobilen Endgeräts zu kopieren und zu speichern. Auch wenn Sie “nichts zu verbergen zu haben”, befinden sich auf dem Gerät nicht nur IHRE Daten, sondern auch die Ihrer Liebsten (Ihrer Kinder, Enkel, Freunde, Kollegen etc.). Sie sollten sich nicht das Recht herausnehmen, auch deren Privatsphäre auf’s Spiel zu setzen. Wenn Sie in eines dieser Länder reisen, wäre es also eine gute Idee hier auf ein Zweitgerät ohne Daten zu setzen.

* Bei sensiblen Unterlagen (wie Pässen, Reisedokumente etc.) das Risiko abwägen, dass man damit eingeht, wenn man diese kopiert oder digital abspeichert. Im Zweifel bringt die Kopie nichts. Sie müssen ohnehin zur Botschaft, wenn Sie die Dokumente verlieren. Wenn Sie dennoch etwas in eine Cloud laden möchten, dann verschlüsseln Sie diese Inhalte. Alternativ bleibt noch ein selbst verschlüsselter USB-Stick oder eine Festplatte mit z. B. [VeraCrypt](www.veracrypt.de).

* Bei Cloud-Diensten meiden Sie kommerzielle Lösungen wie Dropbox, OneDrive, Google, Amazon und Co. Setzen Sie auf Open Source Produkte wie Nextcloud. Es gibt gute Alternativen wie [Mailbox](mailbox.org) oder Managed Nextcloud Instanzen, wenn man nicht selbst hosten möchte. Auch bei Open Source Produkten sollte man zusätzlich verschlüsseln. Hierfür bieten sich 7zip, für vereinzelte Dokumente mit starkem Passwort, oder [cryptomator](https://cryptomator.org/) für [Nextcloud](https://nextcloud.com/) an.

*  Kaufen Sie sich für Ihre EC-Karten, Ihren Personalausweis und Reisepass RFID-/NC Schutzhüllen. Sie verhindern das Auslesen aus dem Umfeld. Die Hüllen gibt es meist nur im gut sortiertem Fachhandel. Alternativ haben wir hier [einen Shop](https://www.pass-sicherheit.de/) gefunden. Wir können zwar damit nicht verhindern, dass manche Länder meinen, man müsste mit ausrangierten Smartphones Reisepässe auslesen, aber die Hüllen verhindern zumindest, dass es das Umfeld tut. Übrigens: Auch Ihre Hotelzugangskarten können funken, packen Sie diese am besten auch in die entsprechenden Schutzhüllen. :)

* Wenn Sie nach Hause telefonieren wollen nutzen Sie am besten: Mumble, Jitsi oder Nextcloud Talk. Diese Dienste können entweder selbstgehostet werden oder Sie zahlen eine kleine Gebühr an Hoster oder Vereine, die den Service ohne Datensammelwut zur Verfügung stellen.

* Schießen Sie Ihre Selfies oder Urlaubsfotos von sich und Ihren Liebsten am besten mit einer Digitalkamera, statt mit dem Smartphone. Hier können Sie sicher sein, dass sich keine Apps (Cloud Sync usw.) Zugang zu Ihren Fotos verschaffen.

* Wenn Sie ein öffentliches WIFI/WLAN verwenden, rufen Sie nichts ungeschützt (ohne VPN) ab. Es gibt immer wieder Berichte über manipulierte Router o.ä. Die AVM Fritzbox bietet inzwischen auch ohne großes technisches Verständnis für Laien die Möglichkeit einen VPN-Zugang selbst anzulegen, um eine sichere Verbindung zu Ihnen nach Hause herzustellen. In vielerlei Hinsicht ist es besser einen VPN selbst zu betreiben. Die Aussagen, von meist dubiosen Anbietern, ala “Wir sind sicher” sollte man sich nicht verlassen. Auch deutsche VPN-Anbieter bieten nicht so viel Sicherheit, wie sie versprechen. Wir möchten an dieser Stelle keine Empfehlung für einen VPN Anbieter aussprechen.

* Wir würden am liebsten empfehlen, Ihren Koffer komplett abzuschließen. Jedoch ist es so, dass Sie dann im Zweifel ein aufgebrochenes Schloss haben, da der Zoll Ihren Koffer öffnen darf. Ihr Koffer sollte sicherheitshalber ein TSA-Schloss haben, ansonsten riskiert man dass der Zoll den Koffer aufbricht. Ein TSA-Schloss kann mit einem Generalschlüssel am Zoll geöffnet werden. Diese TSA Schlüssel werden allerdings [auch von Kriminellen eingesetzt](https://www.heise.de/newsticker/meldung/Generalschluessel-fuer-Gepaeck-Weiterer-TSA-Master-Key-veroeffentlicht-3277250.html), also empfehlen wir nichts  privates oder von Wert im Koffer mitzuführen.

---

### III. Open Source Apps (Android) für die Reise<a name="III._Open_Source_Apps_für_die_Reise"></a>

Da die meisten kaum auf die digitalen Helferlein verzichten wollen, haben wir eine Liste mit Apps aus dem alternativen AppStore [F-droid ](http://www.f-droid.org)zusammengestellt, welche sich als datenschutzfreundliche und sichere Alternative herausgestellt haben.


* [Tricky Tripper](https://f-droid.org/en/packages/de.koelle.christian.trickytripper/) - Reisekosten managen
* [Transportr](https://f-droid.org/en/packages/de.grobox.liberario) - Reisezeiten der Bahn und des öffentlichen Nahverkehrs (fast europaweit)
* [OsmAnd](https://f-droid.org/en/packages/net.osmand.plus) - Alternative zu Google Maps (funktioniert auch Offline)
* [OpenVPN for Android ](https://f-droid.org/en/packages/de.blinkt.openvpn/)- für Hotel und öffentliches WLAN
* [WireGuard](https://f-droid.org/en/packages/com.wireguard.android/) - für Hotel und öffentliches WLAN
* [To Do List](https://f-droid.org/en/packages/io.gitlab.allenb1.todolist/) - Aufschreiben, was gesehen werden soll..
* [Nextcloud](https://f-droid.org/en/packages/com.nextcloud.client/) - Wenn es denn schon unbedingt eine Cloud sein muss..
* [Nextcloud Talk](https://f-droid.org/en/packages/com.nextcloud.talk2) – Telefonierfunktion
* [Plumble](https://f-droid.org/en/packages/com.morlunk.mumbleclient) - ein Mumble Client um per VOIP in Kontakt zu bleiben
* [Conversations](https://f-droid.org/en/packages/eu.siacs.conversations) oder [Pixart](https://f-droid.org/en/packages/de.pixart.messenger) - um mit Freunden und Familie per XMPP zu schreiben
* [Flashlight](https://f-droid.org/en/packages/com.simplemobiletools.flashlight/) - einfache Taschenlampe
* [DictionaryForMIDs](https://f-droid.org/en/packages/de.kugihan.dictionaryformids.hmi_android/) - Wörterbuch
* [Unit Converter](https://f-droid.org/en/packages/com.physphil.android.unitconverterultimate/) - Währungsumrechnung

Natürlich sollte AFWall+ und ein entsprechend gerootetes Gerät nie fehlen - am besten mit aktiver Verschlüsselung. Für nicht gerootete Geräte, oder nicht technisch versierte, ist [Netguard](https://f-droid.org/en/packages/eu.faircode.netguard/) oder [Blokada](https://f-droid.org/en/packages/org.blokada.alarm) zu empfehlen, um den Datenverkehr wenigstens etwas zu beherrschen!

Weitere Apps (allerdings nicht getestet) finden Sie [hier.](https://search.f-droid.org/?q=travel&lang)

---

### IV. Flugzeug-WLAN / Hotel-WLAN - Verzichten Sie besser darauf!<a name="IV._Flugzeug-WLAN_/_Hotel-WLAN_-_Verzichten_Sie_besser_darauf!"></a>

Immer mehr Flugzeuge sind heutzutage mit WLAN ausgestattet. Momentan ist dies noch ein teureres Vergnügen. Über die Technik dahinter macht sich kaum einer Gedanken.

[Wo sind welche Anbieter/Netze aktiv?](http://www.airliners.de/wie-internet-flugzeug-hintergrund/41308)  

* Für die Mittelstrecke / Europa: European Aviation Network (EAN). Hier agiert die Deutsche Telekom (zumindest auf “Lufthansa”-Flügen Angebot: [Flynet](https://www.lufthansa.com/de/en/flynet)). Flynet ist in Kooperation mit dem amerikanischen Unternehmen Panasonic Avionics Corporation entstanden.

* Für Langstreckenflüge: Langstrecke: [GX Aviation](https://www.inmarsat.com/service/gx-aviation) / [Inmarsat](https://www.inmarsat.com/) (Britisches Unternehmen / Datenschutz bei Brexit ist noch relativ unklar)  

In den USA ist das amerikanische Unternehmen [Gogo Inc. / Gogo Inflight Internet](https://www.gogoair.com/) überwiegend aktiv. Auch die europäische Airline [Iberia](http://concourse.gogoair.com/questions-answered-airlines-offer-gogo-flight-internet/) ist mit Gogo ausgestattet.

Inwiefern die Anbieter wechseln, z.B. wenn man mit der deutschen Telekom in Deutschland startet und in den USA landet sprich, dass die Daten dann an Gogo übergehen und somit auch die Nutzerdaten dorthin fließen, ist uns nicht bekannt. 2014 kam heraus, dass GoGo gefälschte SSL-Authentifizierungen nutze und somit eine Man-in-the-middle-Attacke für die User ermöglichte. Auch ist in einem Brief bekannt geworden, dass GoGo mehr Informationen über die Nutzer mit Behörden teilt, als das es rechtlich notwendig wäre.

Doch auch das [WLAN der Deutschen Bahn](https://blog.avast.com/de/ungesichertes-wlan-in-der-deutschen-bahn) ist längst nicht so sicher wie von von vielen gedacht. Daher würden wir auch nur im Notfall raten dies zu benutzen. Auch ein Beitrag des [Chaos Computer Club](https://hannover.ccc.de/~nexus/dbwifi/) rät davon ab.

Eine Alternative ist für viele hier eine Prepaid-SIM-Karte mit Internetpaket für das gewünschte Land. Hier müssen Sie meist sehr viele personenbezogene Daten angeben, in einem Land, was - wenn außerhalb Europas - sich an keinerlei Datenschutzbestimmungen halten muss, wie wir sie kennen. Inzwischen werden die Angaben bei der Prepaid-Registrierung geprüft, weswegen Sie auch im Laden Ihren Personalausweis dabei haben sollten.

Was kann das im schlimmsten Fall heißen? Auswertung und Verkauf Ihres gesamten Surfverlaufes, namentlicher Eintrag im Telefonbuch vor Ort, Spamanrufe usw. Informieren Sie sich also vorab über Angebote und verzichten Sie lieber auf nicht-anonyme Prepaidkarten.
Auch raten wir auf Grund von Googles zahlreicher Datenschutzskandalen von dessen internationaler Sim-Karte ab.

---

### V. Visum<a name="V._Visum"></a>

Wenn Sie Wert auf Datenschutz legen, dann lesen Sie genau **was** Sie unterschreiben, wenn Sie sich für ein Visum bewerben.

Manche Länder verlangen für die Visumsbewerbung die Anlage eines Onlineaccounts. Ob diese Accounts je wieder gelöscht, oder automatisch entfernt werden, ist uns leider nicht bekannt. Auch wissen wir nicht, welche Daten über uns bei Einreise in Länder außerhalb der Europäischen Union erfasst und gespeichert werden. In einem Interview mit [Ulrich Kelber](https://anoxinon.media/blog/interview_kelber/) äußerte sich dieser auch zu dem Thema.

> Zitat:  “Konkrete datenschutzrechtliche Hinweise für individuelle Länder zu veröffentlichen ist leider nicht realisierbar. Denn wir haben keine Erkenntnisse, wie die Datenverarbeitung konkret erfolgt. Der Bfdi bietet jedoch einen [Flyer](https://www.bfdi.bund.de/SharedDocs/Publikationen/Faltblaetter/Reisen.pdf?__blob=publicationFile&v=13), um sich zu informieren.“

Einige Länder außerhalb der EU verlangen sehr sensible Informationen, welche nicht europäischen Datenschutzrichtlinien unterliegen bzw. demnach kein Datenschutz gewährleistet ist. Diese Daten können im schlimmsten Fall gehackt, weiterverkauft oder behördlich weiterverwendet werden. In diesem Worst-Case-Scenario wird Ihre Identität gestohlen, Sie dürfen nicht mehr reisen usw.

Gängige Fragen, die man beim Beantragen eines Visums beantworten muss:

 * Vor- und Nachnamen, Geburtsnamen, Namensänderungen
 * Geburtstag, Geschlecht, Adresse
 * Passnummer (und zusätzlich andere Passnummern, die auf eine Person ausgestellt wurden) / Ablaufdatum
 * Land, in dem der Pass ausgestellt wurde
 * Beziehungsstatus oder ob man jemals in einer ernsten Beziehung war
 * Angaben über Eltern und Geschwister
 * Ist man berufstätig, wenn ja wo? Wer ist der Vorgesetzte? Wie ist dessen Telefonnummer?
 * Reisen Sie mit Familienmitgliedern? Wenn ja, wie heißen diese und in welcher Beziehung stehen Sie zu diesen
 * Haben Sie Verwandte im Land? Wenn ja, nennen Sie deren Namen, Geburtstag, Nationalität/Duldungsstatus, in welcher Beziehung stehen Sie zu diesen?
 * Haben Sie Freunde im Land? Wenn ja, nennen Sie deren Namen, Geburtstag, Nationalität/Duldungsstatus, in welcher Beziehung stehen Sie zu diesen?
 * Ausführliche Informationen zum Gesundheitszustand
 * Beantwortung zu Fragen zum Charakter
 * Social Media Angaben (Facebook-Name, Instagram-Name, Twitterinformationen, WhatsApp Informationen etc.)
 * Aufenthaltsdauer im Land
 * Ausübung eines Berufs oder Weiterbildung im Land

Meist findet sich irgendwo auch versteckt eine allgemein gehaltene Information zu diesen Angaben. Wie oben bereits erwähnt: Sie entscheiden, ob Ihnen das die Reise wert ist. Gerade im Hinblick auf Informationen Dritter, wie Angaben zu Ihren Vorgesetzten, deren Telefonnummern, Angaben zu Familienmitgliedern mit personenbezogenen Daten oder Daten von Freunden, sollten Sie diese vorher, allein aus Respekt und guten Ton, um Erlaubnis bitten. Schließlich haben Sie der Datenspeicherung und -weitergabe, wenn Sie diese einfach in ein Formular eintippen und verschicken, nicht zugestimmt und vielleicht möchten sie das auch überhaupt nicht.

---

### VI. Schlussgedanken<a name="VI._Schlussgedanken"></a>

Hinterfragen Sie die Notwendigkeit von Angeboten, halten Sie Ausschau nach datenschutzfreundlichen Alternativen und vor allem: Prüfen Sie Aussagen, anstelle diesen blind zu vertrauen. Sollten Sie noch weitere datenschutzfreundliche Reisetipps haben, lassen Sie uns das gerne wissen. Falls es demnächst also etwas verantwortungsvollere Tipps im Internet gibt, freuen wir uns davon zu lesen.

Zu diesen zwei Artikeln über den Datenschutz im Tourismus sei weiterführend ein Gastbeitrag, auf dem [Kuketzblog](https://www.kuketz-blog.de/kommentar-der-glaeserne-passagier-die-zukunft-des-reisens/) über die Zukunft des Reisens, erwähnt.

PS: Wir haben bei unseren Recherchen auch einen empfehlenswerten Airline-/Reise-Kanal auf Mastodon entdeckt: https://chaos.social/@fly_it :)
