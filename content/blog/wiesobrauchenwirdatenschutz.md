+++
title = "Wieso brauchen wir Datenschutz?"
date = "2019-01-02T13:00:46+02:00"
description = "Was ist Datenschutz und wieso wird er dringender denn je benötigt? Wir möchten in diesem Beitrag kurz und knackig erklären, wieso kein Dienst im Internet kostenlos ist, welche Daten im Hintergrund gesammelt werden und warum es grundsätzlich jeden betrifft."
tags = ["Anfänger"]
banner = "/img/thumbnail/wieso_brauchen_wir_datenschutz_thumbnail.png"
categories = ["Datenschutz"]
+++

> Dieser Beitrag ist für Anfänger geeignet

### I. Allgemeines ###

Die rasante Entwicklung von Internet- und Kommunikationsdiensten bietet viele Vorteile: Vor einigen Jahren war es entweder kaum möglich oder sehr teuer, wenn Sie von Berlin aus Freunde in den entlegensten Ecken der Welt erreichen wollten. Heute ist das sehr leicht und ohne zusätzliche Gebühren möglich, wenn beide Seiten einen Internetanschluss haben.
Mit der stetig größer werdenden Vernetzung  entstehen auch in zunehmender Geschwindigkeit große Datenmengen, die in vielen Beteiligten, beispielsweise Unternehmen und Behörden, große Begehrlichkeiten wecken, diese Daten bestmöglich für die eigenen Zwecke nutzen. Datenschutz wird meist als lästig und Innovationsbremse bezeichnet. Ist dem so?

### II. Geld ist die Welt ###

Dienstleistungen müssen sich finanzieren. Internetdienste und die Technik dahinter können, je nach Größe, eine große Menge Geld in Anspruch nehmen. Den Internetnutzern wurde beigebracht zu erwarten, dass alle Dienstleistungen, wie E-Mail, Streaming etc. kostenfrei sind. Werbung, mit der sich die Dienste finanzieren, ist zur Normalität geworden. Logischerweise schränkt eine Bezahlschranke von Plattformen auch gleichzeitig die freie Verfügbarkeit von Inhalten ein, es ist also keine wirkliche Lösung.

Nachdem verschiedene weitere Modelle in der Vergangenheit wenig Erfolg gezeigt hatten, haben sich viele Betreiber/Firmen dafür entschieden, individualisierte Werbeanzeigen an den Nutzer auszuspielen, um den bestmöglichen Erfolg zu erzielen.

Hier taucht das erste Problem auf: Wie finde ich heraus, was den Nutzer interessiert? Freiwillig wird er es nicht preisgeben oder gar Falschangaben machen.
Am simpelsten ist es, die digitalen Spuren der Nutzer zu analysieren. Sei es der Verlauf, die Dauer des Besuchs, das Gerät, das Kaufverhalten, Websitenutzung, Suchanfragen, Wohnort, Geschlecht, Alter, sexuelle Orientierung und vieles mehr.
Es wird ständig versucht, dem Nutzerprofil noch weitere wichtige Daten hinzuzufügen. Dazu zählen unter anderem das Einkommen, Gesundheitsdaten und sein soziales Umfeld.

Wenn Sie eine Website aufrufen, sind häufig Drittanbieter und Trackingdienste eingebunden. Viele Zugriffe erfolgen unbemerkt und die Auswirkungen sind auf Anhieb nicht ersichtlich. Hier wieder der Vergleich: Sie sitzen im Café, jedoch hören Ihnen alle rundherum zu und notieren, kategorisieren und bewerten Ihr Verhalten.
Selbst mit aktiviertem Werbe-Blocker gibt es noch genügend andere unbekannte Zuhörer.

Überspitzt könnte man auch von dem dichtesten Spionagenetzwerk seit dem Kalten Krieg reden: Auf eine Person kommen meistens dutzende Agenten. Die Nutzer vertrauen darauf, dass diese nichts Böses damit anstellen und früher oder später alles vergessen.

### III. Wieso Sie doch etwas zu verbergen haben ###

Warum gibt es dennoch so viele Menschen, die angeben, dass sie nichts zu verbergen hätten? Ein Teil der Zugriffe auf Ihre Daten erfolgt unbemerkt und die Auswirkungen sind nicht auf Anhieb ersichtlich und liegen weiter in der Zukunft.
Stellen sie sich einmal vor: Es klingelt an Ihrer Haustür und ein Vertreter der Firma BrillenGlas (frei erfunden) stellt sich vor. Er bietet Ihnen einen tollen Deal an: Sie erhalten „kostenfrei“ ein Tagebuch, einen Kalender, ein Adressbuch, einen neuen glänzenden Briefkasten und einen Notizblock.  Als Gegenleistung möchte er nur zu jeder Tages- und Nachtzeit einen Blick hineinwerfen dürfen und Ihre Nutzung analysieren.

Das wollen Sie nicht? Das ist verständlich und diese Reaktion bedarf keinerlei Erklärung. Dasselbe gilt, wenn die Deutsche Post heute ankündigen würde, sämtlichen Briefverkehr zu öffnen, um Ihnen zielgerichtete Angebote machen zu können, der Arbeitgeber mit Ihrem Arzt über Ihre Krankheit spricht oder der Schwimmbadetreiber Sie in der Umkleidekabine per Videokamera aufnimmt.

Es gibt beispielsweise Kfz-Versicherungsverträge, die einen Rabatt gewähren, wenn Sie die Fahrzeugdaten, die während der Fahrt anfallen, an die Versicherungsgesellschaft übertragen. Wie ist das zu bewerten?  
Wenn Sie immer vorbildlich fahren, könnten Sie zum Entschluss kommen, dass sie nichts zu verbergen haben - im Gegenteil, Sie sparen Geld! Vorstellbar ist aber auch, dass diese Entwicklung im Ganzen und auf Dauer dazu führt, dass diese Daten auch an andere Firmen und Behörden übertragen werden. Künftig kommt vielleicht der Strafzettel in Echtzeit aufs Smartphone? Ist das Panikmache oder ist das gerecht, weil ja dann nicht nur die bestraft würden, die erwischt werden?
Seitens des Gesetzgebers und der Industrie wird des öfteren mit der "Verbesserung des Nutzererlebnisses", "Finanzierung der Dienste" und „Daten als neues Öl, das nicht ungenutzt liegen bleiben darf“ argumentiert. Rechtfertigt das die massenweise Erstellung von Persönlichkeitsprofilen, die meist schlecht gesichert sind und großes Potenzial zur missbräuchlichen Verwendung bieten?

### IV. Fazit ###

Zusammengefasst kann man sagen: Daten bedeuten Verantwortung. Datenschutz ist wichtig und zwar nicht nur dann, wenn es Sie selber betrifft! Sie haben das Recht, sich vor unberechtigten Zugriffen zu schützen und Maßnahmen zu ergreifen, um ein selbstbestimmtes Leben führen zu können, sowohl heute als auch in Zukunft!

Wir werden die angeschnittenen Thematiken in weiteren Beiträgen näher erörtern. Begleiten Sie uns auf eine Reise, in der wir verständliche Erklärungen, Alternativen, sowie Denkanstöße liefern möchten. ;)
