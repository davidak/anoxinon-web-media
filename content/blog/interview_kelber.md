+++
title = "Ulrich Kelber im Interview"
date = "2019-04-14T11:50:00+01:00"
tags = ["Anfänger"]
categories = ["Datenschutz"]
banner = "/img/thumbnail/interview_kelber.png"
description = "Wir haben den Bundesdatenschutzbeauftragten zu verschiedenen Themen befragt, unter anderem über das Fediverse, Videoüberwachung und Freie Software."
+++
<img src="/img/thumbnail/kelber.jpg" alt="Bildnachweis: Bundesregierung - Steffen Kugler" width="150"
height="225" style="float:right; margin-left: 15px;" />

Heute gibt es keinen regulären Artikel, sondern ein Interview mit dem Bundesdatenschutzbeauftragten Ulrich Kelber. Wir haben ihn zu einigen Themen befragt die unsere Redaktion beschäftigen.

Ulrich Kelber trat das Amt zum Bundesbeauftragten für den Datenschutz und die Informationsfreiheit im Januar 2019 an und löste damit Andrea Voßhoff ab. Er studierte Informatik und Biologie und setzt u. a. in seinen privaten Onlineauftritten auf das Fediverse. Bevor er Bundesdatenschutzbeauftragter wurde, war er parlamentarischer Staatsekretär im Justizministerium.

<small>Bildnachweis: Bundesregierung - Steffen Kugler</small>

<hr>

**Herr Kelber...**

**Was halten Sie persönlich von dem Ansatz, Linux in öffentlichen Einrichtungen zu
nutzen und welche Meinung vertritt der Informatiker in Ihnen beim Thema "freie
Software"?**

*Freie Software bietet eine Reihe von Vorteilen: Technologische Autonomie, Unabhängigkeit in der Entwicklung der behördlichen IT-Landschaft und bessere Durchsetzbarkeit von Datenschutz- und Datensicherheitsanforderungen. Die Weichen müssen so gestellt werden, dass immer häufiger freie Software und Anwendungen auf Basis freier Software eingesetzt werden können.*

**Datenschutz endet im digitalen Zeitalter nicht an den Grenzen des Bundeslandes
oder Europas. Was möchten Sie in Ihrer Funktion als Bundesdatenschutzbeauftragter tun, um ungewollte Veröffentlichungen von personenbezogenen Daten außerhalb des Europäischen Wirtschaftsraumes zu verhindern?**

*Ungewollte Veröffentlichungen werden sich nie vollumfänglich verhindern lassen.
Die Datenschutz-Grundverordnung (DSGVO) ist allerdings ein hervorragendes
Werkzeug, um das Risiko solcher Veröffentlichungen zu minimieren. Denn die
DSGVO entfaltet gleich auf zwei Wegen datenschutzrechtliche Strahlwirkungen
über die Grenzen der EU hinaus: Zum einen führt das Marktortprinzip dazu, dass
auch Unternehmen in Drittländern sich an das europäische Recht halten müssen,
wenn sie entweder ihre Dienstleistungen oder Produkte auf dem europäischen
Markt anbieten, oder im Auftrag von europäischen Unternehmen Daten verarbeiten. Zum anderen zeigt sich, dass die DSGVO immer öfter von anderen Ländern
als Benchmark für eigene Datenschutzgesetzgebung herangezogen wird. In Kalifornien tritt beispielsweise ein neues Datenschutzrecht in Kraft, das sich an die DSGVO anlehnt. Auch in Japan und Lateinamerika orientiert man sich an den hiesigen Vorschriften. Die DSGVO entwickelt sich insoweit immer mehr zum weltweiten Goldstandard für den Datenschutz.*

**Sie haben sich bereits einige Male explizit zum Thema "connected car" geäußert.
Welche Pläne haben Sie, um die Automobilhersteller in die Schranken zu weisen,
damit wir künftig nicht den gläsernen Bürger auch noch in der Automobilindustrie
vorfinden?**

*Aufgrund der föderalen Struktur der Datenschutzaufsicht in Deutschland sind die
jeweiligen Landesdatenschutzbeauftragten für die Aufsicht über die Automobilhersteller zuständig. Ich berate die Regierung und das Parlament bei einschlägigen
Gesetzgebungsverfahren, wie zuletzt der Änderung des Straßenverkehrsgesetzes
zur Ermöglichung des automatisierten Fahrens. Außerdem führt der BfDI bereits
seit Jahren einen Dialog mit dem Verband Deutscher Automobilhersteller. In diesem Rahmen haben wir beispielsweise ein gemeinsames Positionspapier entwickelt. Dieses ist auf [meiner Website](https://www.ulrich-kelber.de) veröffentlicht, ebenso wie der vom BfDI initiierte Beschluss der internationalen Datenschutzkonferenz zum [automatisierten und vernetzten Fahren](https://www.bfdi.bund.de/SharedDocs/Publikationen/Allgemein/DatenschutzrechtlicheEmpfehlungenVernetztesAuto.html;jsessionid=5C31447DE078D330276E9EF11B75E0C4.2_cid354?nn=5217154). Da wir hier aber noch relativ am Anfang dessen stehen, was in
Zukunft möglich sein wird, werde ich das Thema weiterhin eng begleiten.*

**Es kommt leider auch immer öfter vor das deutsche Unternehmen als Adresshändler tätig werden. Als Beispiel nehmen wir die Deutsche Post und Partnerunternehmen, bei denen man einen Widerspruch einlegen kann, aber diese sich trotzdem das
Recht vorbehalten Werbung zu verschicken. Auskunfteien oder Datensammler (z. B.
Mastercard mit Google-Offline-Tracking) machen Umsatz mit dem An- und Weiterverkauf von personenbezogenen Informationen, ohne dass der Kunde viel davon
mitbekommt. Der Betroffene fühlt sich hier oft machtlos, da Anfragen wegen Überlastung der Behörden im Sande verlaufen. Wie planen Sie solche Probleme künftig
zu lösen bzw. die Unternehmen mehr in die Verantwortung zu nehmen?**

*Grundsätzlich gilt, dass ein Werbewiderspruch auch beachtet werden muss. Hierzu muss der Widersprechende allerdings bereit sein, sich in eine entsprechende
Blacklist eintragen zu lassen. Tut er das nicht, sondern verlangt die Löschung seiner Daten, kann es tatsächlich passieren, dass er erneut Werbung von demselben
Unternehmen erhält. Diese Gefahr besteht beispielsweise immer dann, wenn er
nach wie vor bei einem Adresshändler gelistet ist.
Unabhängig davon sehe ich die dringende Notwendigkeit, die Vorschriften zum
Scoring und zur Profilbildung zu verschärfen. Gerade letztere ist eine wesentliche
Grundlage, auf der vor allem personalisierte Werbung platziert wird. Aufgrund der
immer weiter fortschreitenden Digitalisierung produzieren wir auch immer mehr
Daten, die, miteinander verknüpft, oft eine sehr hohe Aussagekraft haben. Hier
müssen wir zwingend bei der Evaluierung der DSGVO im kommenden Jahr datenschutzrechtlich nachbessern.*

**Wie sehen Sie die Konzentrierung von Datensammlungen auf einige wenige "Big
Player" wie Google, Microsoft und Co. (bzw. die sogenannten Big Five)?**

*Ich sehe dies äußerst kritisch. In den Anfangstagen des Datenschutzes ging es
vornehmlich um die Gefahren hoheitlicher Datenverarbeitung. Auch wenn die Bedeutung von Datenschutz gegenüber dem Staat – gerade im Sicherheitsbereich –
nach wie vor hoch ist, sind mindestens genauso große Risiken in der Privatwirtschaft entstanden, man kann mit Fug und Recht davon sprechen, dass sich eine
Art digitaler Überwachungskapitalismus entwickelt hat. Daher sehe ich es als eine
unserer wichtigsten Aufgaben an, die DSGVO gegenüber den großen IT-Giganten
um- und durchzusetzen.*

**Uns ist natürlich nicht entgangen, dass Sie privat das Fediverse (Mastodon) aktiv
nutzen. Wir begrüßen die Entscheidung, dass Amtsträger auch dezentrale Alternativen aktiv nutzen. Was halten Sie von dem Ansatz der Dezentralität bei Onlinediensten im Internet?**

*Dezentrale Angebote wie GNU Social und Mastodon haben gegenüber zentral betriebenen, kommerziellen Angeboten einen klaren Vorteil: sie unterliegen nicht den
kommerziellen Interessen des jeweiligen Betreibers. Dieser Umstand bedeutet
auch, dass sie in den meisten Fällen eine datenschutzfreundlichere Alternative zu
kommerziellen Angeboten darstellen. Leider fristen sie oft immer noch ein Nischendasein, da ihre Akzeptanz – bedingt durch die mangelnde Bekanntheit –
schlichtweg nicht groß genug ist. Trotz oder gerade wegen dieser Herausforderungen versuche ich, dezentrale Angebote durch meine Präsenz zu unterstützen.*

**Ihr Mastodon Account erweckt den Eindruck, dass Sie gerne Reisen. In Bezug auf
das Ausland haben viele Länder ein unterschiedliches Datenschutzniveau. Die USA
hat zum Beispiel beim Antrag auf ein Besuchervisum ganz klar formuliert: "Your data will be property of the United States. You cannot expect privacy!", ohne transparent zu machen, was das im Zweifel bedeutet. Das formulieren andere Länder ähnlich. Haben Sie Pläne, den Bürgern entsprechende Informationen bereitzustellen,
bspw. gemeinsam mit dem Auswärtigen Amt?**

*Konkrete datenschutzrechtliche Hinweise für individuelle Länder zu veröffentlichen
ist leider nicht realisierbar. Denn wir haben keine Erkenntnisse, wie die Datenverarbeitung konkret erfolgt. Allerdings findet man auf der Website des BfDI grundsätzliche datenschutzrechtliche Informationen zum Thema Reisen. Hier kann man
sich beispielsweise den Flyer „[Datenschutz an der Grenze und auf Reisen](https://www.bfdi.bund.de/SharedDocs/Publikationen/Faltblaetter/Reisen.pdf?__blob=publicationFile&v=13)“ herunterladen, der wichtige datenschutzrechtliche Aspekte dieser Thematik beleuchtet.*

**Wie stehen Sie zur Quellentelekommunikationsüberwachung und Onlinedurchsuchung?**

*Es handelt sich bei beiden Maßnahmen um erhebliche Grundrechtseingriffe, die
dementsprechend sowohl technisch als auch vor allem rechtlich verhältnismäßig
ausgestaltet werden müssen. Gerade bei der Rechtsgrundlage hatte das Bundesverfassungsgericht erhebliche Bedenken. Auf diese hat der Gesetzgeber im Wesentlichen mit entsprechenden Gesetzesanpassungen reagiert. Allerdings sind
einzelne Regelungen – insbesondere zur "kleinen" Onlinedurchsuchung, die innerhalb von Maßnahmen zu Telekommunikationsüberwachungen möglich ist – teilweise aus meiner Sicht nach wie vor verfassungsrechtlich problematisch.
Unabhängig davon erkennen wir einen klaren Versuch, die Kompetenzen der Sicherheitsbehörden in diesen Bereichen immer umfassender zu gestalten. Wir fordern hier klare Grenzen und versuchen, als Aufsichtsbehörde zu erreichen, dass
die Grenzen eingehalten und die Rechte der Betroffenen gewahrt bleiben.*

**Apropos, gerne wird mehr Videoüberwachung als Mittel gegen Kriminalität und Terrorismus in der politischen Diskussion gefordert. Wie stehen Sie zur Videoüberwachung allgemein und zur Ausweitung eben jener, z.B. zur Videoüberwachung am Berliner Südkreuz?**

*Sicherlich gibt es Bereiche in denen eine Videoüberwachung im rechtlich zulässigen Rahmen durchaus sinnvoll ist. Allerdings sehe ich eine große Gefahr, wenn
die Videoüberwachung im öffentlichen Raum so flächendeckend ausgeweitet würde, dass man ständig das Gefühl haben müsste beobachtet zu werden. Wenn
Menschen deshalb anfangen, selbst von legale Aktivitäten wie die Teilnahme an
einer Demonstration Abstand zu nehmen, ist eine Grenze überschritten, die auch
das Bundesverfassungsgericht immer als klare rote Linie festgelegt hat.
Projekte wie am Bahnhof Südkreuz, bei denen Videobilder automatisiert biometrisch ausgewertet werden, haben noch einmal eine ganz andere Eingriffsintensität
und sind daher gesondert zu bewerten. Aktuell fehlt es hier vor allem an entsprechenden Rechtsgrundlagen. Ob und wie solche überhaupt verfassungskonform
ausgestaltet werden können wird sich noch zeigen müssen.*


**Abschließend noch eine Frage: Wo sehen Sie den größten Handlungsbedarf beim
Thema Datenschutz und Informationsfreiheit?**

*Wie bereits erwähnt müssen wir besonders bei den Regelungen zur Profilbildung
und zum Scoring nachbessern. Die anstehende erste Evaluierung der DSGVO bietet dafür eine gute Gelegenheit. In diesem Zusammenhang sollten wir auch prüfen,
inwieweit wir das Gesetz entbürokratisieren können, ohne dabei das Datenschutzniveau zu schleifen.
weiterer Handlungsbedarf besteht nach wie vor im Bereich des Beschäftigtendatenschutzes und natürlich bei der E-Privacy Verordnung. Gerade letztere, die den
sehr wichtigen weil sehr sensiblen Bereich des Datenschutzes bei der Telekommunikation und Internetnutzung regeln soll, muss nun zügig zu einem Abschluss
gebracht werden.
Im Bereich der Informationsfreiheit müssen wir noch mehr dafür tun, die Arbeit des
öffentlichen Sektors transparenter und damit für Bürgerinnen und Bürger besser
verständlich zu machen. Ich würde mir dabei wünschen, dass Behörden Dokumente, die sie im Falle einer Anfrage nach dem Informationsfreiheitsgesetz ohnehin
herausgeben müssten, bereits eigeninitiativ veröffentlichen. Um hier mit gutem
Beispiel voranzugehen, habe ich bereits den Auftrag erteilt, ein entsprechendes
Konzept für den BfDI zu entwickeln.*

**Vielen Dank! :)**

<hr>
