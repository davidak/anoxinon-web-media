+++
title = "Datenschutz im Tourismus - Teil 1"
date = "2019-05-22T12:30:00+02:00"
tags = ["Anfänger"]
categories = ["Datenschutz"]
banner = "/img/thumbnail/Tourie_Flieger1.png"
description = "Wer freut sich schon auf den nächsten Urlaub? Sonne, Strand, Meer oder doch lieber in die Berge? In dieser Artikelserie möchten wir Ihnen näher bringen, was in der Tourismus Branche schief läuft und wohin Ihre Daten sonst noch reisen."

+++

> Trotz sorgfältiger Recherche behalten wir uns Irrtümer vor.

Die Urlaubszeit naht und viele werden sich früher oder später fragen, ob man überhaupt datenschutzfreundlich eine Reise buchen kann. Worauf sollte man achten? Was ist wichtig? In dem Beitrag "[Die Macht der Datensammler](https://anoxinon.media/blog/diemachtderdatensammler/)" haben wir bereits erwähnt, warum die Einbindung von Drittanbieterskripten und Trackern gefährlich sein kann.

Ob Buchungsportale wie [Expedia](https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.expedia.de), Vermittlungsportale wie [Trivago](http://www.trivago.de) oder direkt beim Hotel: Wir gehen hier vorzugsweise auf die direkte Hotelbuchung ein, da bei der Buchung über Vermittlungsportale zu viele Daten gestreut werden. Unser Ziel ist die Datenminimierung. Daher, auch wenn es Rabatte gibt, bedenken Sie, dass dort die Buchungsdaten durch weitere Hände gehen. Zusätzlich dazu muss man dann auch noch sicher sein, dass der Anbieter verantwortungsvoll mit den
Daten umgeht.

---

<img src="/img/thumbnail/Tourie_Flieger1.png" class="fullsize" />

---

**Inhaltsverzeichnis:**  

[I. Hotelbuchung](#I._Hotelbuchung)  
[II. Transport](#II._Transport)  
[III. Geld sparen, Meilenkonten, Bahncard und weitere Bonusprogramme](#III._Bonusprogramme)  
[IV. Schlussgedanken](#IV._Schlussgedanken)  

---

### I. Hotelbuchung <a name="I._Hotelbuchung"></a>
Wir starten mit der direkten Hotelbuchung über das Internet. Wie handhaben das die meisten Hotels? Je nach Hotel gibt es verschiedene Ansätze:

  * [Kontaktformular](#kontaktformular)
  * [E-Mail](#email)
  * [Webbuchungsportal auf der Hotelwebsite (eigenes oder Fremdportal)](#webbuchungsportal)
  * [Buchung im Reisebüro](#reisebuero)
  * [Telefonische Buchung](#telefon)
  * [Vor Ort](#vorort)
  * [AirBnB](#airbnb)

**Kontaktformular:**<a name="kontaktformular"></a>   

Auch wenn es gewisse Regelungen gibt, dass z. B. Formulare nicht unverschlüsselt personenbezogene Daten senden dürfen, hält sich leider nicht jeder daran.
Zusätzlich ist es für den Endverbraucher meist nicht ersichtlich, inwiefern diese sicher konfiguriert wurden oder ob das Formular durch einen Drittanbieter bereitgestellt wird.

**E-Mail Buchungen:** <a name="email"></a>   

Im Beitrag „[DSGVO für Profis](https://anoxinon.media/blog/dsgvo_fuer_profis/)“ sind wir auf das Thema Datenminimierung und den Grundsatz der Integrität und Vertraulichkeit eingegangen. Viele Unternehmen erfüllen noch nicht die Anforderungen der DSGVO (seit Mai 2018 gültig) und haben sich zusätzlich zu den wenigsten Prozessen und deren Auswirkungen Gedanken gemacht. Gerade wenn Unternehmen keine große IT-Abteilung haben oder der Ansicht sind, dass muss „nur“ funktionieren, sieht die Umsetzung düster aus. Hotels haben z.B. meist keine spezielle IT-Abteilung, da der Kernbereich die Dienstleistung am Gast ist. Leider scheinen sich wenige Hotelbetreiber Gedanken um die Handhabung Ihres E-Mail Verkehrs gemacht zu haben, insbesonders ob diese DSGVO-konform ist.

Gerade in Frankreich und Griechenland gibt es viele Hotels, die wie selbstverständlich orange.fr, googlemail.com, yahoo.com oder ähnliche Anbieter für Zimmerbuchungen nutzen, anstatt den E-Mail Server selbst zu hosten. In den seltensten Fällen wird mit, oben genannten, Drittanbietern überhaupt ein Auftragsverarbeitungsvertrag geschlossen. Aber auch, wenn Hotels den E-Mail Server selbst hosten und dafür auf verschiedene Dienstleister gesetzt haben, kann es vorkommen, dass die Daten in die USA geschickt werden. Zudem werden Buchungs-E-Mails oft unbefristet gespeichert. Dort befinden sich Namen, Geburtstag, Adresse und ggf. auch Kreditkarteninformationen. Das Ganze wandert dann, unter Umständen aus steuerrechtlichen Gründen, in ein E-Mail-Archiv und wird für sehr lange Zeit nicht mehr gelöscht. Ein Löschkonzept existiert hier meist nicht. Außerdem kann es sein, dass viele E-Mails über mehrere Jahre im Posteingang verweilen. Im Falle eines Hackerangriffes könnten hier also einige Informationen abgegriffen werden.

Wie die Hotels ihre E-Mails handhaben, kann man mit [www.hardenize.com](www.hardenize.com) oder [www.privacyscore.org](www.privacyscore.org) prüfen. Bitte beachten Sie, dass keine personenbezogenen Daten eingeben werden sollten. Hierfür reicht die Domain, z. B. www.superhotel.de (fiktives Beispiel).

**Webanwendungen:**<a name="webbuchungsportal"></a>

Eine weitere Möglichkeit zur Buchung, ist die Webanwendung. Hier kann das Hotel, im besten Falle, auf eine eigene Webanwendung setzen, die ohne Drittanbieter auskommt. Diese Verbindung kann man mit oben genannten Links prüfen. Alternativ nutzt das Hotel ein Webbuchungsportal wie z. B. cbooking, dirs21, guestcentric, synix oder reserve-online.net, welches Ihre Daten auf den eigenen Servern speichert.

Alle der oben genannten Webbuchungsmanager enthalten bei der Eingabe von personenbezogenen Daten und Kreditkarteninformationen Tracker. Bei Portalen wie Guestcentric (USA) oder reserve-online.net ist dies kaum verwunderlich und ein Sicherheitsrisiko für Ihre personenbezogenen Daten. Aufgrund fehlender Kontaktdaten konnten wir den Firmensitz von Reserve Online nicht eindeutig ausfindig machen. Sollte der Sitz jedoch im türkischen Teil von Zypern sein, gilt die DSGVO dort nicht. Leider sind auch bei cbooking und dirs21, als deutsche Anbieter, einige Tracker im Buchungsformular integriert. Dies ist gerade bei der Eingabe von sensiblen personenbezogenen Daten, sowie der Bankverbindung oder Kontaktdaten ein Risiko. Wir haben alle genannten Anbieter mehr als vier Wochen im Voraus des Artikels kontaktiert und nur von dirs21 ein Statement erhalten.

**Wie finde ich heraus, was das Hotel nutzt?**

Für den Laien ist dies nicht hundertprozentig erkenntlich. Es gibt jedoch gute Indizien: Zum einen kann man, sobald man auf „Zimmer buchen“ klickt, prüfen, ob sich die Adresse in der Browserzeile verändert hat. Man besucht bspw. die Seite superhotel.de (frei erfunden) und nach dem Klick auf “Hotel buchen” ändert sich die Domain in cbooking.superhotel.de. Dann nutzt das Hotel die Webanwendung von cbooking.

Was ist denn nun schon wieder das Problem mit Webbuchungsportalen? Auch dort sind viele Drittanbieter aktiv. Die Gefahren haben wir bereits ausführlich erklärt, aber auch weitere Risiken können hier auftreten:

  *  Unsichere Konfiguration der Webbuchungsanwendung bzw. dadurch bedingte unverschlüsselte Übertragung oder Speicherung von Daten.
  *  Man muss auch darauf vertrauen, dass die Person (z. B. die Reservation des Hotels), die das Portal verwaltet, ein sicheres Passwort gewählt hat und dieses auch regelmäßig ändert. Zusätzlich dazu muss das Portal auch gut vor Brute-Force-Angriffen geschützt werden, sowie Passwörter sicher verschlüsseln (bzw. hashen).
  *  Je nachdem, wo der Webbuchungsmanager seinen Sitz hat, werden die Daten im Ausland (ggf. auch mit niedrigem oder keinem Datenschutzniveau) gespeichert. Dazu zählt auch, wenn das Unternehmen zwar in Europa ansässig ist, jedoch Partnerunternehmen in einem Nicht-EU Land hat. Wir können nicht hundertprozentig sicher sein, dass dort der Datenschutz, trotz der DSGVO, praktiziert wird.
  *  Nicht existente automatische Löschroutinen, also die unbegrenzte Speicherung von Daten. Manuell wird höchstwahrscheinlich kein Hotelangestellter die Daten durchgehen und löschen.
  * Wo große Datenmengen sind, wird auch gerne angegriffen. Bestes Beispiel: [Synix und der Sabre Hack](https://www.forbes.com/sites/leemathews/2017/07/06/travel-giant-sabre-confirms-its-reservation-system-was-hacked/).
  *  Es gilt die Datenschutzerklärung der Webbuchungsanwendung und nicht die der Hotelwebsite. Dies ist meist überhaupt nicht ersichtlich, da viele Hotels nicht einmal erwähnen, dass die Daten an einen Webbuchungsmanager gesendet und dort gespeichert werden. Über den Einsatz der Tracker wird selbst im Buchungsformular oft nicht ausreichend hingewiesen.

Da dies ein sehr abstraktes Thema ist, haben wir eine Mindmap angefertigt, in der wir es vereinfacht grafisch dargestellt haben.

<img src="/files/tourismus1/buchungsmanager.jpg" class="fullsize" />

Sollten Sie bei einem Hotel Ihrer Wahl die Entdeckung von zahlreichen Trackern im Buchungsformular machen, sprechen Sie dieses doch einmal aktiv darauf an. Nur wenn man Menschen darauf aufmerksam macht, werden sich die verantwortlichen Personen Gedanken machen. Bei den Webbuchungsmanagern sind wir leider nicht sonderlich weit gekommen, da uns kaum geantwortet wurde. Wir können Ihnen nur raten, solange so viele Tracker aktiv sind und kein Löschkonzept existiert, diese nicht zu benutzen.

Gerne können Sie bei, Anfragen an Hotels, unser Formular nutzen:

<br><div style="text-align: center">  <a href="/files/tourismus1/Anfrage_Hotelbuchung_Datenschutz.pdf" class="btn btn-small btn-template-main">Download Hotel Formular</a></div><br>

Für alle Hotels bzw. Hotelbesitzer und interessierten Gäste haben wir ein PDF-Dokument erstellt, welches das Thema Datenschutz bei Hotels-/Hotelketten durchleutet und die Schwachstellen, sowie Verbesserungsmöglichkeiten aus unserer Sicht aufzeigt.

<br><div style="text-align: center">  <a href="/files/tourismus1/Hotel.pdf" class="btn btn-small btn-template-main">Download Hotel</a></div><br>

Passend zu diesem Thema empfiehlt es sich [auch diesen Bericht von Symantec](https://www.symantec.com/blogs/threat-intelligence/hotel-websites-leak-guest-data) zu lesen. Dieser zeigt auf das 2 von 3 Hotels (unfreiwillig) Daten über ihre Gäste leaken.

**Buchung im Reisebüro:** <a name="reisebuero"></a>   

Wir gehen hier ausschließlich auf die Buchung in einem regulären Reisebüro ein. Also nicht auf spezielle Firmendienst-Reisebüros. In regulären Reisebüros findet die Buchung (fast) ausschließlich persönlich vor Ort statt.

Bei der Buchung der Reise muss man unterscheiden, ob es sich um eine Leistung eines Veranstalters handelt oder um eine Leistung direkt vom Leistungsträger. Bei der Reisebuchung wird zwischen Buchung beim Veranstalter (Tui, Dertour etc.) und Buchung beim Leistungsträger unterschieden. Bei der Buchung beim Veranstalter erhält man meist eine ausgedruckte Bestätigung. Die finalen Unterlagen werden ein paar Wochen später dem Reisebüro per Post zugesandt. Hier kann der Kunde dann meist wählen, ob er diese postalisch erhält oder diese selbst abholt. Kleiner Hinweis, [Tui schickt seit ein paar Jahren (nach Onlineinformationen) außschließlich digitale Unterlagen](http://https://www.heise.de/newsticker/meldung/Tui-schafft-gedruckte-Reiseunterlagen-ab-Kritik-von-Reisebueros-3502899.html) - inklusive Kundenlogin, was bei einer Einmalbuchung nicht gerade datenschutzfreundlich ist!

Meist sind bei einem Reisebüro mehrere Systeme im Einsatz. Hierbei handelt es sich oft um Buchungsplattformen wie [Amadeus](http://www.amadeus.com) (Europäischer Sitz mit deutschem Datencenter). Jedoch hatte [Amadeus 2019 einen erhebliches Sicherheitsproblem](https://www.borncity.com/blog/2019/01/16/schwachstelle-beim-amadeus-flugbuchungssystem/). Aber auch [Sabre](http://www.sabre.com) (Sabre wurde vor einigen Jahren [erfolgreich mit einem Hack um mehrere Onlinekonten erleichtert](https://www.travelweekly.com/Travel-News/Travel-Technology/Sabre-SynXis-hotel-reservations-system-hacked)) und [Galileo](http://www.travelport.com) (mit Sitz in UK) sind sehr stark vertreten. Um uns über die aktuellen Datenschutz- und Sicherheitsgedanken der Hersteller zu informieren, haben wir alle drei vor Veröffentlichung kontaktiert und keiner hat reagiert oder wollte sich dazu äußern. Übrigens: Wenn Sie eine Flugreise buchen, sind Sie höchstwahrscheinlich in einem dieser drei Systeme erfasst - egal wo Sie buchen.

Zusätzlich können auch weitere Plattformen hinzukommen (wie z. B. [BISTRO](http://www.amadeus.com/web/amadeus/de_AT-AT/Reiseb%C3%BCros/Unsere-Produktpalette/Serve/Relevante-Angebote-bereitstellen-/AT_DE_Amadeus-TT-BistroPortal/1319526448919-Solution_C-AMAD_ProductDetailPpal-1319632955512?industrySegment=1259068355825&level2=1319608963678&level3=1319609097018)) für weitere Leistungen am Gast. Hierbei handelt es sich jedoch meist um ein separates System. Wie die Buchung (gerade im Bezug auf die Hotelbuchung) weiterverläuft, ist von Reisebüro zu Reisebüro unterschiedlich und hängt auch von den eingesetzten Portalen ab. Die Buchung beim Hotel kann dann durch den Reisebüro-Mitarbeiter entweder per E-Mail oder telefonisch erfolgen.
Bei einer Plattform wie Amadeus, wird die Buchung via Schnittstelle direkt in die Buchungssoftware im Hotel (z. B. OPERA) eingepflegt.

Ob in Reisebüros noch Pässe kopiert werden, ist uns nicht bekannt. Jedoch lohnt es sich, die Betreiber darauf anzusprechen, ob eine Notwendigkeit einer Kopie besteht und nicht das Vorzeigen genügt. Bedenken Sie bitte, Ihre Informationen, die nicht benötigt werden, bei einer Kopie zu schwärzen! Normalerweise sollte ein Vorzeigen des Passes jedoch ausreichen.
Fazit: Buchung im Reisebüro kann sich lohnen, man muss aber aktiv nachfragen, ob das Reisebüro Ihrer Wahl auch anständig mit Ihren Daten umgeht! Auch wenn Sie keine Kreditkarte haben oder Ihre Zahlungsinformationen nicht dem Hotel bekannt machen möchten, können Sie in einigen Reisebüros in Bar bezahlen (einfach nachfragen!)
Aber Augen auf, manche Reisebüros (z. B. Alltours) bieten auch die Möglichkeit mit einfachsten (schlecht gesicherten Mitteln) seine Buchung einzusehen (Stichwort: Webcode) und zu stornieren.
Die Gefahr mit den einfachen Anmeldeverfahren (Webcodes) wird bei den Flugreisen ausführlicher erklärt.

Und sollten Sie in einem Reisebüro arbeiten haben wir noch:  

<br><div style="text-align: center">  <a href="/files/tourismus1/Worauf_Reisebüros_in_Sachen_Datenschutz_achten_sollten.pdf" class="btn btn-small btn-template-main">Download Reisebüros</a></div><br>

**Telefonische Buchung:** <a name="telefon"></a>   

Auch in diesem Fall muss man den zweiten Schritt bei der Buchung im Hintergrund beachten: Die verwendete Software im Hotel selbst. Das ist natürlich nicht mehr das Webbuchungsportal, sondern eine weitere Software. Je nachdem in welches Land Sie reisen, kann es sein, dass man Sie a) auf Grund von Sprachbarrieren schlecht versteht oder b) die Festnetznummer auf ein privates Smartphone weitergeleitet wird. Wenn auf diesem Smartphone Informationen gespeichert werden und neugierige Apps aktiv sind, liegen Ihre Daten ggf. wiedereinmal ungewollt bei der Werbeindustrie. Es könnte sein, dass Ihr Anruf ungewollt aufgezeichnet wird. Klären Sie im Vorfeld ab, ob dies der Fall ist oder nicht und entscheiden Sie dann, ob Sie Ihre persönlichen Informationen sowie Bank-/Kreditkarteninfos weitergeben möchten.

**Vor Ort:**<a name="vorort"></a>    

Mit etwas Glück kann man auch spontan vor Ort buchen. Auch in diesem Fall muss man den zweiten Schritt bei der Buchung im Hintergrund beachten.

Selbstverständlich können auch im Hotel Techniken genutzt werden, die die Privatsphäre gefährden, z. B. "smarte Hotelräume”, “personalisierte Smartphones für Gäste”, “Touchpads im Zimmer” usw. Achtung Ironie: Gerade, wenn man selbst viel tut, um googlefrei zu leben, “freut” man sich natürlich sehr, wenn jemand anderes Datensätze für Sie anlegt.

**AirBnB:**<a name="airbnb"></a>

Wir möchten an dieser Stelle natürlich noch AirBnB erwähnen, da dieser Dienst oft als “Alternative” genannt wird. Wir empfehlen Airbnb nicht. Warum? Daten werden sowohl bei Airbnb selbst gespeichert, sowie an Werbenetzwerke weitergegeben. Ggf. werden weitere Daten an den Vermieter versandt. Ein privater Vermieter muss sich nicht einmal zwingend an die DSGVO halten. Er kann die Gästedaten via Googlemail und Co. empfangen und auch leichtsinnig speichern.

---

### II. Transport <a name="II._Transport"></a>
Weiter geht es mit den Transportmöglichkeiten zum Hotel. Da kaum einer mehrere hunderte Kilometer zu Fuß gehen möchte, wird er wohl auf einem der nachfolgenden Wegen reisen:

* [Flugzeug](#flugzeug)
* [Bus](#bus)
* [Bahn](#bahn)
* [Mietwagen](#mietwagen)
* [Eigenes Auto](#eigenesauto)

**Flugzeug:**<a name="flugzeug"></a>

Das anonymes Fliegen bei der Fülle an personenbezogenen Informationen, die an Behörden übergeben werden, nicht möglich ist, sollte jedem bewusst sein. Dennoch gibt es Airlines, die es mit Datenschutz ernster nehmen als andere.

Was sind unsere Kriterien bei der Wahl einer Airlines?

  * Ein Kundenkonto ist bei der Buchung keine Voraussetzung
  * Bei der Buchung selbst sind wenige bis keine Tracker aktiv
  * Europäische Airlines, die sich an die DSGVO halten
  * Viele personenbezogene Daten sind optional
  * Der E-Mail Dienst ist selbstgehostet

Hierfür haben wir uns die Mühe gemacht, große europäische Airlines zu prüfen und auch mit der Thematik zu konfrontieren. Unsere Ergebnisse, unsere Erklärungen zu den Kriterien und natürlich die Antworten der Airlines, haben wir in unserem “Airline Ranking” zusammengefasst.

 <br><div style="text-align: center">  <a href="/files/tourismus1/Airline_Ranking.pdf" class="btn btn-small btn-template-main">Download Airline Ranking</a></div><br>

Was bei vielen Airlines negativ auffällt, ist die Einseh- und Stornierbarkeit von Flügen anhand des Nachnamens und der Buchungsnummer. Dies birgt einige [Sicherheitsrisken](https://www.zeit.de/digital/datenschutz/2016-12/reisedaten-hacking-betrug-buchungscode-33c3), wie z. B., dass ohne große Hackingkenntnisse Ihre personenbezogenen Daten eingesehen werden können, Ihr Flug geändert bzw. storniert werden kann und ggf. die Möglichkeit besteht die Kontaktdaten zu ändern. Über eine Fremdeinwirkung haben Sie kaum Nachweise. Auf Anregungen dies zu ändern, haben Airlines bis heute nicht reagiert oder diese Möglichkeit aus Sicherheitsbedenken deaktiviert.

Während unserer Recherchen zur Airline-Datensicherheit wurde im Februar 2019 bekannt, dass Sicherheitsforscher anhand des Phänomens, den Verbraucher schnell Zugang zu den Buchungsdaten ohne Passwort zu gewähren, ein sogar noch größeres Sicherheitsproblem entdeckt haben. Sieben Airlines, trauten wohl den Kunden nicht einmal zu, den Namen und die Buchungsnummer einzugeben und verschickten unverschlüsselte Links, welche kein Passwort etc. zur Identifizierung benötigten. Bei Bekanntwerden des Links haben bzw. hatten Dritte die Möglichkeit, auf Name, Adresse, Reisepassinformation, Kreditkartendaten etc. zuzugreifen. Auch war es möglich, Boardkarten auszudrucken, Flüge umzubuchen und Reisen zu stornieren. Die Funktionen, welche einsehbar waren bzw. immer noch sind und was genau bei welcher Airline möglich war, hat sich von Airline zu Airline unterschieden. Wandera hat einen Sicherheitscheck durchgeführt und diesen [hier](https://www.wandera.com/mobile-security/airline-check-in-risk/)  (inkl. Airlinenamen) veröffentlicht. Ein paar dieser Airlines haben wir selbst mit Datenschutzanfragen/IT-Sicherheitsfragen kontaktiert und keine Antwort erhalten.

---

<img src="/files/tourismus1/Ticket3.png" class="fullsize" />

---

Der Chaos Computer Club hat auf das Problem der Buchungsnummern und Nachnamen als Authentifizierung bereits 2016 hingewiesen. Es existiert ein Video, welches auch für Laien sehr leicht verständlich ist und das sich jeder Mitarbeiter/Verantwortliche der Tourismusindustrie ansehen sollte. Da dies aber nicht reicht, liegt es auch an den Lesern/Menschen, selbst aktiv zu werden und die Hersteller darauf anzusprechen.  
**Empfehlenswerter Videolink:** [Where in The World is Carmen San Diego?](https://media.ccc.de/v/33c3-7964-where_in_the_world_is_carmen_sandiego)

Die Systemhersteller Galileo, Amadeus und Sabre (eines hiervon ist bei jeder Flugbuchung aktiv) haben wir mit einem eigenen Fragenkatalog kontaktiert und um ein Statement gebeten. Auch hier haben wir weit über 4 Wochen vor Artikelveröffentlichung, Zeit für eine Antwort gegeben. Von keinem der drei kam ein Statement. Kurz vor unserer Veröffentlichung hat auch [Caschys Blog](https://stadt-bremerhaven.de/zwei-von-drei-hotels-fehlt-es-an-sicherheit-fuer-ihre-kundendaten) das Thema angeschnitten.  

**Bus:**<a name="bus"></a>

Leider ist es mit Flixbus derzeit nicht möglich anonym zu buchen. Bei der Buchung im Flixbus Reisebüro kann man zwar ohne Tracker die Tickets erwerben, jedoch werden die Tickets personalisiert ausgegeben und personenbezogene Daten im System von Flixbus gespeichert. Wir wissen nicht, wie diese gespeichert werden und wie lange. Die Flixbus-Website selbst, ist leider stark mit Trackern etc. versehen, sodass wir die Buchung darüber nicht empfehlen. Ohne Angabe von personenbezogenen Daten kann z.B. der IC Bus der Deutschen Bahn gebucht werden. So kann man noch datenschutzfreundlich reisen.

**Bahn:**<a name="bahn"></a>

Bahntickets können auch heute noch anonym vor Ort am Automaten oder in einem Reisezentrum der Bahn erworben werden. Bei Flixtrain ist die Buchung vor Ort etwas anders. Man kann zwar, wie bereits unter ‘Bus’ erwähnt, in einem Flixtrain Reisebüro buchen, jedoch nicht wirklich anonym. Die Flixbus bzw. Flixtrain Website selbst ist leider stark mit Trackern etc. versehen, sodass wir die Buchung darüber nicht empfehlen. Fazit: Die Bahn ist zwar teurer, bietet aber vor Ort noch eine Möglichkeit anonym und bar sein Zugticket zu erwerben (solange man keine Bahncard beantragt, bleibt man auch anonym).

**Mietwagen:**<a name="mietwagen"></a>

Nahezu jeder Mietwagen wird heute via GPS getrackt. Somit können die zurückgelegten Strecken genau nachverfolgt werden. Aber auch wen das nicht stört bzw. wer es für den Urlaub verkraften kann, weil er/sie eine Rundreise plant, sollte in Sachen Datenschutz bei der Wahl des Mietwagens aufpassen. Vorab sollte man ein paar Informationen einholen: Wird der Führerschein/Pass kopiert oder nur eingesehen? Wie viele Tracker sind aktiv? Kann ich vor Ort buchen? Zusätzlich kann man bei dem Anbieter die Praktiken erfragen.

**Eigenes Auto:**<a name="eigenes auto"></a>

Datenschutzfreundlich reist es sich auch mit dem eigenen PKW, solange Sie noch kein smartes Modell besitzen. Das einzige, was aus Sicht des Datenschutzes bedenklich wäre, sind Kennzeichenscans in manchen Ländern beim Grenzübergang sowie die Videoüberwachung.

---

### III. Geld sparen, Meilenkonten, Bahncard und weitere Bonusprogramme <a name="III._Bonusprogramme"></a>

Früher oder später wird die Frage aufkommen, was wir von Bonusprogrammen halten. Kurzgefasst: Nichts. Grundsätzlich gelten nachfolgende Absätze für alle Bonusprogramme.

Miles & More, und ähnliche Anbieter, tun sehr viel, um Sie dazu zu bringen mehr und mehr Daten preiszugeben. Sie spielen oft mit der Angst, etwas zu verlieren.

Bis auf berufliche Vielflieger dürften viele Teilnehmer des Bonusprogramms von Miles & More, durch Flüge alleine, kaum in den Genuss attraktiver Prämien kommen. Daher werden zusätzliche Anreize geschaffen, um weitere Punkte zu sammeln: Kooperation mit anderen Bonusprogrammen, sowie der Einsatz der Miles & More Kreditkarte. Der Haken an der Sache: Man meldet sich bei einem Programm an, um eine attraktive Prämie zu bekommen, merkt aber, dass die gesammelten Punkte nicht ausreichen. Da die Punkte auch nur begrenzt gültig sind und man das angesparte “Guthaben” nicht verfallen lassen möchte, nutzt man weitere Optionen, um zusätzliche Punkte zu sammeln. Das hat wiederum zur Folge, dass man deutlich mehr Daten an Miles & More sowie deren Partner in Drittstaaten überträgt.

Dank des Payback Programms und der Kooperationen zwischen verschiedenen Anbietern, lässt sich ein ausführliches Persönlichkeitsprofil erstellen. Damit nicht genug: Einer von Deutschlands größten Adresshändlern (Deutsche Post) vergütet mit seinem Produkt “Leserservice”, bei Abschluss eines Abos und dem "Werben eines Freundes", Payback-Punkte. Durch zahlreiche Blogs, “Travel Hacks” und Co. wird man dazu aufgefordert, daran teilzunehmen. Man kommt an die heiß begehrten Punkte und darf sich über extra Meilen freuen. PaybackPunkte kann man übrigens 1:1 in Meilen umwandeln.

In den meisten Fällen werden für Sie, bei solchen Payback Programmen, nur ein paar Euros rausspringen, oder Sie bekommen ein schickes Badetuch mit Werbung des Anbieters. Im Gegenzug dafür liegen Ihre personenbezogenen Daten bei Miles & More, der deutschen Post, Payback und wahrscheinlich sehr vielen anderen Werbepartnern weltweit.

Übrigens: Von einer Bahncard und dem zugehörigen Bonusprogramm Bahn Bonus möchten wir abraten. Im Gegensatz zu Miles & More gibt es bei der Bahn sehr viel weniger Möglichkeiten “Punkte” zu sammeln, aber dadurch fährt man nicht mehr anonym und übergibt somit der Bahn die Möglichkeit, das persönliche Fahrverhalten auszuwerten.

> Hinweis: Vor Veröffentlichung haben wir Miles & More kontaktiert. Der Redaktionsschluss war Miles&More bekannt. Wir haben leider keinerlei Stellungnahme erhalten.


### IV. Schlussgedanken <a name="IV._Schlussgedanken"></a>

Wir hoffen mit diesem Beitrag einen guten Einblick in das [Thema Datenschutz oder Daten-nicht-Schutz im Tourismus](https://m.biztravel.fvw.de/reise-management/neue-biztravel-serie-datenschutz-im-travel-management/1/169869/17230) gegeben zu haben. Es kann nicht sein, dass wir 2019 noch Systeme nutzen, die sich seit den [70er/80er Jahren kaum verändert haben](https://www.heise.de/newsticker/meldung/33C3-Gravierende-Sicherheitsluecken-bei-Reisebuchungssystemen-3582423.html). Es kann auch nicht sein, dass die einzigen Lösungsansätze sind, in Software mehr und mehr Drittquellen oder Tracker einzubinden und wir somit ein ähnlich schlechtes System haben wie zuvor. Und nein, es kann auch nicht sein, dass bei der Entwicklung neuer System hauptsächlich “Marketingexperten” eingebunden werden, oder IT-Personal ohne entsprechende Sicherheitskenntnisse.

Die Hersteller bzw. die Industrie sollte dringend ihre Sicherheitsstandards überdenken, dazu gehört:

* Kein Aufdrucken der Buchungsnummer auf Gepäckaufklebern, Boardkarten. Auf Gepäckaufklebern und Boardkarten ist ebenso der Nachname abgebildet. Buchungscode + Nachname ist alles, was man benötigt, um eine Buchung zu ändern, stornieren oder personenbezogenen Daten einzusehen.
* Zugriffsbeschränkungen auf die Buchungsdaten. Nicht jeder darf unlimitierten Zugang zu Buchungsdaten bekommen.
* Zugriffe müssen aufgezeichnet werden, auch um Straftaten vorzubeugen.
* Zugriff auf die Buchungsdaten nur mit einem starken Passwort oä. erlauben.
* Auf Tracker in den Systemen sollte verzichtet werden.

Es ist aber auch wichtig, dass Sie als Leser aktiv werden. Fragen Sie nach, wenn Sie Sicherheitsfragen oder Datenschutzbedenken haben. Machen Sie von Ihrer Stimme Gebrauch und zeigen Sie, dass Ihnen es nicht egal ist, wie mit Ihren Daten umgegangen wird. Machen Sie sich aber auch bewusst, dass auch wenn Ihnen etwas kritisches auffällt, Sie sehr oft eine Standardemail mit “Wir nehmen Datenschutz sehr ernst” zurückkommt, obwohl dies offensichtlich nicht so ist. Ursprünglich wollten wir noch wenigstens eine Seite finden, die gerade für Flugreisen datenschutzfreundlich und trackerfrei agiert, wenn es um Spenden für die Reduzierung des ökologischen Fußabdruckes geht. Leider können wir nicht keine Seite empfehlen, die unseren Kriterien an Datenschutz entspricht. Wir können nicht alles abdecken und wollen mit diesem Beitrag eher einen “ausführlichen” ersten Einblick in die Problematik geben. Wir freuen uns natürlich auf Ihre Hinweise - ob positiv oder negativ. Falls Sie sinnvolle Vorschläge haben, was die Hersteller hier verbessern können und wie, zögern Sie nicht, uns anzuschreiben.

Danksagung: *Der Start der Artikelreihe hat sehr viel Recherchearbeit von uns abverlangt. Wir danken allen, die sich an diesem Artikel beteiligt haben, den Unternehmen (die sich die Zeit genommen haben auf unsere Datenschutzanfragen ausführlich zu reagieren), den Grafikern und unserem Berater aus der Branche (danke für deine vielen Hinweise und Hilfestellungen)!*
