+++
title = "Ist Freie Software seriös?"
date = "2019-01-02T12:00:00+02:00"
tags = ["Anfänger"]
categories = ["Freie Software"]
banner = "/img/thumbnail/ist_freie_software_seriös_thumbnail.png"
description = "Haben Sie schon einmal den Begriff Freie Software oder Open Source gehört, oder bereits entsprechende Software benutzt? Wie kann sich frei verfügbare Software überhaupt lohnen, ohne dubiose Finanzierungsmethoden?"
+++

> Dieser Beitrag ist für Anfänger geeignet

### I. Was ist eigentlich Freie Software? ###

Der Quelltext von freier Software ist öffentlich und kann von Dritten eingesehen, genutzt und verändert werden. Im Gegensatz zu geschlossenen Systemen (proprietärer Software), wie z. B. von Microsoft und Apple, können Softwarefehler und Sicherheitslücken so schneller geschlossen und durch viele Menschen überprüft werden.

Dieses Konzept geht auf den Grundgedanken des freien Austausches von Wissen zurück.

### II. Wer steckt hinter dem Ganzen? Lohnt sich das überhaupt? ###

Es gibt viele Institutionen die schon sehr lange den Gemeinschaftsgedanken leben und als hochwertige Ressourcen für jede/r Mann/Frau zugänglich sind. Ein langlebiges Beispiel einer nicht-kommerziellen Institution, an der sich jeder Beteiligen kann, ist Wikipedia. Dahinter steht ein gemeinnütziger Verein, kein Unternehmen.

Weit verbreitet sind der Browser Firefox und der E-Mail Client Thunderbird der Mozilla Foundation, welche ebenfalls eine Non-Profit-Organisation ist. Auch das bekannte Bildbearbeitungsprogramm GIMP, ist eine umfangreiche freie Software, hinter der kein Unternehmen steckt.

Aber nicht nur freiwillige Entwickler tun sich zusammen und arbeiten an freier Software, sondern auch Firmen. Diese geben ihre Software an die Gemeinschaft frei, meist mit komplett einsehbaren Code, zum Weiterentwickeln für jeden.
Warum tun das Firmen, wenn Sie damit kein Geld verdienen? Das Geschäftskonzept einer Firma die freie Software verbreitet, stützt sich meist auf den Verkauf von professionellen Dienstleistungen wie Hosting und Support – Die, die Software entwickeln können sie am besten verwalten. Außerdem finanzieren sich manche auch durch Crowdfunding. Diese Stütze ist leider nicht immer stabil, weswegen es schwierig ist sich darauf zu verlassen.

Zudem profitieren Unternehmen von Feedback der Community. Bugs können schneller gefunden werden, somit wird eine aktive und schnelle Weiterentwicklung der Software gefördert. Aus ethischer und finanzieller Sicht ist es durchaus lohnenswert die eigene Software unter einer freien Lizenz zu veröffentlichen. Viele kommerzielle Anbieter sträuben sich jedoch dagegen, da sie den eigenen Code als ihr Geschäftsgeheimnis ansehen.
Auch wird oft das Argument vorgebracht, dass eine Geheimhaltung des Quellcodes die Sicherheit erhöht, dies ist jedoch umstritten.

### III. Was ist der Unterschied zwischen freier Software und Open Source? ###

Oft werden diese beiden Begriffe vermischt, da es Überlappungen gibt, es besteht jedoch ein Unterschied zwischen beiden Konzepten.

- Open Source bezeichnet lediglich, dass der Quellcode öffentlich ist.
- Freie Software (Frei wie in Freiheit) schließt gewisse Rechte mit ein, man kann den Code selber verändern, ausführen und weitergeben – je nach Lizenz

Wir möchten an dieser Stelle noch auf eine <a href="https://www.gnu.org/philosophy/free-sw.de.html">detaillierte Beschreibung</a> des Begriffs Freie Software der GNU Foundation hinweisen.

### IV. Freie Software im Einsatz ###

Heutzutage muss sich freie Software im Design und Funktionsumfang nicht vor der großen Konkurrenz verstecken.
Firmen die stark auf quelloffene Software setzen sind z. B. Red Hat (Fedora), Oracle (MySQL) und Caconical (Ubuntu).

Freie Software steckt zudem in mehr Produkten, als man auf Anhieb denken mag. Tesla setzt in seinen Fahrzeugen stark auf Ubuntu, die meisten Server laufen mit einem Linux-Kernel, Android ist ebenfalls quelloffen und basiert auf Linux, auch wenn viele Hersteller ihre eigenen proprietären Apps mit ausliefern.
<br><br>
Wir möchten die Gelegenheit nutzen und ein paar Projekte erwähnen:<br><br>


- <a href="https://www.gimp.org/">GIMP</a> - Bildbearbeitungsprogramm mit einer riesigen Community und vielen Anleitungen

- <a href="https://www.libreoffice.org/">LibreOffice</a> - Eine umfangreiche Office-Lösung

- <a href="https://www.openstreetmap.org/">OpenStreetMap</a> – Ein Projekt mit dem Ziel eine freie Weltkarte zu erschaffen

- <a href="https://marble.kde.org/">Marble KDE</a> - Freie Satellitenbilder
<br><br>


Es gibt natürlich wesentlich mehr, diese alle einzeln vorzustellen würde aber den Rahmen sprengen. Wir werden nach und nach Beiträge mit weiterführenden Links veröffentlichen.

### V. Schlussworte ###

Hervorzuheben ist, dass die Community um freie Software schon längst aus den „Keller-Nerd“ Zeiten heraus gewachsen ist. Es gibt noch hier und da Knackpunkte, die aber mit gutem Willen lösbar sind.

Einige Behörden setzen bereits auf freie Software. Es entlastet diese nicht nur finanziell, sondern bietet auch weitere Vorteile. Leider ist Deutschland, in diesem Bereich, noch kein Land mit „Vorbildfunktion“. Die Niederlande, Spanien, Italien und zuletzt auch Kanada setzen schon vermehrt auf freie Software in Behörden. <br><br>

Vielleicht wird sich das in Zukunft noch ändern. Wir werden sehen ;)
