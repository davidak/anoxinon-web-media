+++
title = "Das Tor zur Anonymität"
date = "2020-01-20T20:00:00+02:00"
tags = ["Anfänger"]
categories = ["Datenschutz", "Freie Software", "Dezentrale Netzwerke"]
banner = "/img/thumbnail/torbrowser.png"
description = "Um seine Privatsphäre im Internet zu schützen surft man am besten mit Tor. Die Abkürzung steht für „The Onion Router“ – ein Netzwerk, das in mehreren Schichten aufgeteilt ist, wie eine Zwiebel (englisch: Onion). Dies ermöglicht es sich bestmöglich vor flächendeckender Überwachung und Zensur zu schützen. Eine Rückverfolgung, wer wann welche Webseite aufgerufen hat, ist somit unmöglich."
+++

Um seine Privatsphäre im Internet zu schützen surft man am besten mit Tor. Die Abkürzung steht für
„The Onion Router“ – ein Netzwerk, das in mehreren Schichten aufgeteilt ist, wie eine Zwiebel
(englisch: Onion). Dies ermöglicht es sich bestmöglich vor flächendeckender Überwachung und Zensur zu schützen. Eine Rückverfolgung, wer wann welche Webseite aufgerufen hat, ist somit unmöglich.
Der nachfolgende Beitrag soll eine einsteigergerechte Einführung in die Thematiken rings um **Tor** ermöglichen.



---

**Inhaltsverzeichnis:**  

1. [Was ist der Tor-Browser](#torbrowser)  
	
2. [Tor ist kein (normales) VPN](#vpn)  

3. [Surfen per Tor](#surfen)  
	3.1 [Sicherheitshinweise](#sicherheitshinweise)  

4. [Probleme und Lösungen](#probleme_lösungen)  
	4.1 [Problem 1: Bad Exits](#problem1)  
	4.2 [Problem 2: langsam](#problem2)  
	4.3 [Problem 3: Tor-Sperren](#problem3)  
	
5. [Ausblick](#ausblick)  

---


### 1. Was ist der Tor-Browser und was kann man damit machen?{#torbrowser}

Tor ist ein Anonymisierungsnetzwerk. Die Anwendung um dieses Netzwerk zu nutzen wird auch oft Tor genannt, wobei es genau genommen der Tor-Client ist. Diese Anwendung arbeitet als Systemdienst und ist dadurch "nicht sichtbar".

Zusätzlich gibt es den Tor-Browser. Der Tor-Browser ist ein auf [Firefox ESR](https://support.mozilla.org/en-US/kb/firefox-esr-release-cycle) basierender Browser, der
 - neben anderen Optimierungen für die Privatsphäre und die Sicherheit - das Surfen über das
Tor-Netzwerk ermöglicht. Hier wird die ESR-Version von Firefox als Grundlage verwendet, damit die Anpassungen für die Tor-Nutzung seltener an Änderungen von Firefox angeglichen werden müssen. Das kann einem als Nutzer aber egal sein, da der Tor-Browser eine automatische Update-Funktion hat und man als Nutzer nicht mit den Details in Kontakt kommt.

### 2. Tor ist kein (normales) VPN{#vpn}

Bei einem konventionellen [VPN](/anxicon/#vpn) ist man Kunde eines bestimmten Anbieters. Dann nutzt man einen
- meist proprietären - VPN-Client und leitet den gesamten eigenen Internetdatenverkehr über diesen Anbieter. Das setzt viel Vertrauen voraus, da man ein Programm des Anbieters ausführt und der Anbieter den gesamten eigenen Internetdatenverkehr sehen kann.

Tor hingegen nutzt man nur gezielt für bestimmte Anwendungen, wie z.B. den Tor-Browser. Außerdem gibt es dort nicht einen Anbieter, sondern viele verschiedene Relais-Betreiber, die vom Tor-Client zufällig ausgewählt und regelmäßig gewechselt werden. Diese Relais machen jeweils nichts anderes als Daten gemäß Anweisungen weiterzuleiten. Zwischen einer Anfrage eines Tor-Nutzers und dem "normalen Internet" stehen 3 Relais, die alle von verschiedenen Anbietern sind und in verschiedenen Netzwerkbereichen stehen. Dadurch wird es erschwert, dass ein Relais-Betreiber oder ein [ISP](/anxicon/#isp) ermitteln kann, woher der durch Tor anonymisierte Netzwerkverkehr kommt.

### 3. Surfen per Tor{#surfen}

Hierfür ist es wirklich empfehlenswert, den Tor-Browser zu benutzen.

Man kann den Tor-Client verwenden und diesen bei einem beliebigen Browser als Proxy einstellen. Das ist allerdings nicht empfehlenswert. Zum einen ist man sehr auffällig (und damit wiedererkennbar), wenn man Tor-Relais benutzt und nicht den Tor-Browser verwendet. Andererseits wird beim Tor-Browser darauf geachtet, dass die Nutzer möglichst alle gleich aussehen.

Hierzu gibt es einen kurzen [FAQ-Eintrag](https://2019.www.torproject.org/docs/faq.html.en#TBBOtherBrowser).

Durch die Nutzung vom Tor-Browser kann man sich vor privatwirtschaftlichen Datensammlern schützen. Diese "sehen" einen weiterhin, aber die Tor-Browser-Nutzer sehen alle ähnlich aus und sind somit schwer unterscheidbar.

Es ist unklar, wie gut man sich vor staatlichen Einrichtungen mit Tor schützen kann, aber wenn man Tor nicht verwendet ist man definitiv leichter überwachbar.

Per Tor kann man:

* normal surfen ohne Registrierung
* Online-Banking durchführen (kann je nach Bank als "Konto-Hacking" interpretiert werden und das Online-Banking sperren)
* Online-Shopping durchführen (abhängig vom Shop)
* sonstigen Seiten mit Registrierung benutzen
* ...

Hierbei können Tor-Sperren stören - dazu später mehr.

Beim Online-Banking und Online-Shopping ist man trotz Tor selten anonym, da man üblicherweise der Kontoinhaber ist oder man Waren zu sich schicken lässt.

Bei anderen Seiten mit Registrierungspflicht kann man z.T. ohne echte Angaben auskommen. Wenn das möglich ist, ist es empfehlenswert dies zu nutzen. Allerdings sollte man dort sämtliche Handlungen, die Rückschlüsse auf die reale Identität zulassen, vermeiden. Insbesondere empfiehlt es sich die Zugangsdaten, die man per Tor nutzt niemals ohne Tor einsetzen, weil man sonst den Zusammenhang von der eigenen IP-Adresse zu diesem Account preisgibt und da durch die Anonymität des Accounts verliert.

#### 3.1 Sicherheitshinweise{#sicherheitshinweise}

Im Allgemeinen sollte man die Sicherheitslevel Option im Tor-Browser so hoch wie möglich einstellen. Mit einer niedrigeren Stufe surft man bequemer, aber unsicherer. Diese Sicherheitsstufe regelt, welche Funktionen im Browser aktiviert werden und welche nicht.

---
<img src="/img/tor/tor-security-level-menu.png" class="fullsize" />  
<img src="/img/tor/tor-security-level-settings.png" class="fullsize" />

---

Allerdings kann man Funktionen auch selektiv aktivieren, z.B. kann man in der höchsten Sicherheitsstufe per NoScript **gezielt** Skripte aktivieren, wenn das bei der Seite die man nutzen möchte zwingend erforderlich ist und man diese Seite unbedingt nutzen möchte. Dafür ist es möglich im Menü "Customize" zu wählen, um dann NoScript zur Toolbar hinzuzufügen. Da man die Größe des Webseitendarstellungsbereichs damit nicht ändert ist das unbedenklich. Danach kann man zum Erlauben von Skripten in der höchsten Sicherheitsstufe auf das NoScript-Symbol klicken und bei der entsprechenden Domain "temporarily trusted" auswählen:


<img src="/img/tor/tor-noscript-allow.png" class="fullsize" />

Wenn man dann auf den grünen Neu-Laden-Button drückt kann man die Seite benutzen. In diesem Beispiel sieht man auch, dass durch die eingesetzte "DDos Protection" des Unternehmens Cloudflare die Skripte temporär aktiviert werden müssen. Aber zu Cloudflare und Tor-Sperren später in diesem Artikel mehr.

Die Größe des Tor-Browsers und das Zoom-Level sollte man nicht verändern, weil man sich dann von anderen Tor-Benutzern unterscheidet. Genauso ist es empfehlenswert (bis auf das Sicherheitslevel und ggf. das Erlauben von Skripten) **nichts** an den Einstellungen zu verändern, keine Add-ons zu installieren und auch keine Lesezeichen zur Lesezeichen-Symbolleiste hinzuzufügen (da das die Browserfenstergröße verändern würde).

Um eine Datenzusammenführung und Verwechslung zu vermeiden sollte man in einer Tor-Browser-Sitzung jeweils entweder normal surfen oder eine bestimmte Seite mit Registrierungspflicht verwenden. Ansonsten könnte es passieren, dass unnötigerweise wahrgenommen wird, dass es einen Zusammenhang zwischen verschiedenen Konten bei verschiedenen Diensten gibt.

Weiterhin ist davon abzuraten, eine Seite gleichzeitig im Tor-Browser und im normalen Browser zu benutzen. Hier besteht eine hohe Gefahr, dass man die beiden Browser verwechselt. Außerdem könnten Korrelationen auffallen (immer, wenn Nutzer A online ist, ist auch Nutzer B online).

Auch von Bedeutung ist, dass man HTTPS verwendet. Dazu jetzt etwas ausführlicher.

### 4. Probleme und Lösungen{#probleme_lösungen}

#### 4.1 Problem 1: Bad Exits{#problem1}

Der letzte Relais-Knoten (aus der Sicht eines Nutzers, der eine Webseite benutzt) stellt den Übergang vom Tor-Netz zum "normalen" Internet dar. Ab diesem Punkt fließen die Daten so durchs Netz, wie sie es tun würden, wenn man einen normalen Browser benutzt. Ein Bad Exit könnte also alles tun, was auch ein böser Internetanbieter machen könnte: Verbindungen belauschen oder falsche Klone von Webseiten ausliefern. Solange man sich nur Webseiten ansieht ist das unproblematisch. Wenn man eine ausführbare Datei herunterlädt oder sich irgendwo anmeldet ist das sehr ungünstig, weil dann die Verbindung abgehört oder manipuliert werden könnte, sodass man dann eine Schadsoftware anstelle des Programms bekommt oder das die Passwörter in falsche Hände geraten.

<img src="/img/tor/torssl.png" class="fullsize" />

##### Lösung: eine weitere Verschlüsselungsebene

Auch im normalen Internet sollte man Passwörter nie im Klartext übertragen und Programme nicht ungesichert übertragen. Eine mögliche Lösung dafür nennt sich [HTTPS](https://de.wikipedia.org/wiki/Hypertext_Transfer_Protocol_Secure). Bei der Nutzung von HTTPS kann Niemand, der die Daten in der Leitung mit liest, die Inhalte sehen. Daher sollte man, wie bei jeder anderen Internetverbindung auch, darauf achten, dass man sich nicht bei der Domain vertippt, und dass man HTTPS verwendet.

Nutzt man HTTPS, dann sieht auch der Exit-Node nur den verschlüsselten HTTPS-Netzwerkverkehr. Dann weiß er, wohin die Daten gehen. Aber er weiß nicht von wem und was die Inhalte sind, sodass hierbei keine Gefahren bestehen.

Hierzu sei auf den (kurzen) [FAQ-Eintrag](https://2019.www.torproject.org/docs/faq.html.en#CanExitNodesEavesdrop) verwiesen.

#### 4.2 Problem 2: langsam{#problem2}

Die Kapazitäten von Tor sind begrenzt und Verbindungen über Tor nehmen Umwege. Das senkt die Bandbreite und erhöht die Latenz spürbar. Dagegen kann man nur selbst Tor-Relais betreiben oder an einen Relais-Knoten-Betreiber spenden.

Allerdings ist Tor für nahezu jede Anwendung schnell genug.

Auch an dieser Stelle ein Verweis auf einen [FAQ-Eintrag](https://2019.www.torproject.org/docs/faq.html.en#WhySlow).

#### 4.3 Problem 3: Tor-Sperren{#problem3}

Es gibt Webseitenbetreiber, die wollen Sicherheit. Für solche gibt es Dienste, die die Sicherheit der Webseite verbessern sollen. Nun gibt es deutlich weniger Tor-Exit-Nodes als normale Internetanschlüsse. Das hat zur Folge, dass von diesen wenigen Exit-Nodes viele Anfragen kommen. Diese Sicherheitsdienste interpretieren das dann als Angriff und sperren den Zugriff auf die entsprechenden Webseiten. Das Problem dabei ist, dass oft der "globale" Netzwerkverkehr dabei als Grundlage gilt, d.h. die Summe der Anfragen vom Exit-Node an alle durch diesen Dienst geschützten Seiten. Das sind einerseits viele Anfragen, andererseits kommt es wie auch bei normalen Internetanschlüssen manchmal vor, dass ein Exit-Node für böse Handlungen benutzt wird.

Allerdings wird es mit den Sperren oft übertrieben. Früher war Cloudflare bei Tor-Nutzern bekannt, weil man bei (fast) jeder von Cloudflare geschützten Webseite erst ein Google-reCaptcha lösen musste, bevor man die Seite sehen konnte. Inzwischen hat man das Problem dort [wahrgenommen (Achtung: Link zum Cloudflare-Blog)](https://blog.cloudflare.com/the-trouble-with-tor/).

Einer [Erhebung](https://w3techs.com/technologies/details/cn-cloudflare) zufolge wird Cloudflare bei 80,1% aller Webseiten, die einen bekannten Reverse-Proxy-Dienst einsetzen, verwendet, was insgesamt 11,3% der Webseiten entspricht (Stand: Januar 2020). Für die Erbringung des Dienstes entschlüsselt Cloudflare den Netzwerkverkehr, prüft ihn und leitet ihn (abhängig von der Webseite) verschlüsselt oder unverschlüsselt an den Zielserver weiter. Somit kann man Cloudflare als größten [Man-in-the-Middle](https://de.wikipedia.org/wiki/Man-in-the-middle-Angriff) der Welt bezeichnen.

Cloudflare schützt viele Seiten, aber es gibt noch andere Dienstleister, bei denen man auch heute noch als Tor-Nutzer vollständig ausgesperrt wird. Weiterhin gibt es Seiten, bei denen man keinen Verweis auf einen Dienstleister sieht, wo aber dennoch Tor-Nutzer ausgesperrt werden. Das sind wahrscheinlich selbst gebaute Filterlösungen auf der Basis der Exit-Node-IP-Adressen, welche öffentlich einzusehen sind.

Zu diesem Thema gibt es im Tor-Wiki eine [Liste von Seiten, die Tor-Nutzer aussperren](https://trac.torproject.org/projects/tor/wiki/org/doc/ListOfServicesBlockingTor).

### 5. Ausblick{#ausblick}

Mit Tor kann man auch Location Hidden Services betreiben und nutzen, anonym Dateien tauschen sowie Videochats abhalten und Textnachricht versenden. Dieser Artikel soll jedoch für Anfänger sein deshalb werden wir zu diesen Themen einmal mehr zu einem späteren Zeitpunkt bereit stellen.

---
Bilder:  
Tor onion Logo: [Tor Project](https://www.torproject.org/docs/trademark-faq.html.en) [CC BY 3.0 US](https://creativecommons.org/licenses/by/3.0/us/) 




