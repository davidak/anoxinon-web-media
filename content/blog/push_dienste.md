+++
title = "Push-Dienste und Datenschutz"
date = "2019-10-21T20:00:00+02:00"
tags = ["Anfänger"]
categories = ["Datenschutz"]
banner = "/img/thumbnail/Push_Dienste_Thumbnail.png"
description = "Was sind eigentlich Push-Dienste? Wozu brauchen wir sie und was sind ihre Aufgaben? Gibts es dazu Datenschutzbedenken? Der folgende Beitrag erklärt die Grundlagen zu Push-Diensten und welche Probleme damit einher gehen."

+++
> Trotz sorgfältiger Recherche behalten wir uns Irrtümer vor. Dies ist keine Rechtsberatung. Bei juristischen Fragen wenden Sie sich bitte an einen fachkundigen Anwalt.

Es gibt Dienste, die man bewusst benutzt. Ein Beispiel dafür ist eine Suchmaschine wie [Qwant](https://www.qwant.com/). Im Hintergrund gibt es Dienste, die man nicht sieht und nicht bewusst nutzt - die Push-Dienste. Um diese soll es im folgenden Beitrag gehen.

Push-Dienste werden von einem externen Dienstleister bereitgestellt. Sie benötigen Daten, um Ihren Push-Dienst anbieten zu können.


---

**Inhaltsverzeichnis:**  

1. [Grundlagen](#grundlagen)  
	1.1 [Clients und Server](#clientsundserver)  
	1.2 [Was ist Push?](#wasistpush)  
	1.3 [Wie läuft ein Push ab?](#wieläuftpushab)  
	
2. [Das Ressourcenproblem](#ressourcenproblem)  
	2.1 [Der Umgang mit diesem Problem](#umgang)  
	
3. [Die üblichen Push-Dienste](#push-dienste)  

4. [Die Probleme](#probleme)  
	4.1 [Das Zentralisierungsproblem](#zentralisierung)  
	4.2 [Das Datenschutzproblem](#datenschutzproblem)  
	4.3 ["Besserung in Sicht ?!"](#besserung)  
	
5. [Andere Arten von Push-Diensten - Fazit](#fazit)  

---
### 1. Grundlagen{#grundlagen}
<br>

#### 1.1 Clients und Server{#clientsundserver}  
<br>

<img src="/img/pushdienste/grafik_1.png" class="fullsize" />

<br>
Der Client ist die Anwendung / App / Komponente, die man als Nutzer bei sich (auf dem Computer, Smartphone, in der vernetzten Glühbirne, etc. ) installiert hat.

Der Server ist die Gegenstelle für den Client. Diese Gegenstelle wird vom Anbieter eines Produktes / Dienstes (z.B. vom E-Mail-Anbieter, Hersteller der vernetzten Glühbirne, Suchmaschinenanbieter, ...) oder einem von ihm beauftragten Dienstleister zur Verfügung gestellt.

Das ist der Regelfall. Es muss aber nicht so sein, da man auch selbst einen Server bei sich betreiben könnte. Aber wer so etwas macht, weiß bereits, was Clients und Server sind. Es gibt auch andere Sonderfälle, die bei dieser Erklärung vernachlässigt wurden.

#### 1.2 Was ist Push?{#wasistpush}

Viel wichtiger ist es aber ersteinmal zu klären, was Push sein soll. Um in Clients neue Inhalte zu erhalten, gibt es im grundlegenden zwei Ansätze: Entweder ruft der Client regelmäßig die aktuellen Inhalte ab (Pull-Ansatz), oder der Client wartet darauf, dass er vom Server Inhalte zugesendet / gepusht bekommt (Push-Ansatz). Der Push-Ansatz hat den Vorteil, dass Inhalte sofort an den Empfänger gehen. Das ist bei Kommunikationsanwendungen wie [Conversations](https://conversations.im/#security) üblicherweise wünschenswert.

#### 1.3 Wie läuft ein Push ab?{#wieläuftpushab}

Push-Nachrichten können ohne Zusatzdienstleister direkt vom Server des Dienstes an den Client gesendet werden. Dafür baut der Client eine Verbindung auf, meldet regelmäßig, dass er noch da ist, und wartet auf Nachrichten vom Server. Im Gegensatz zum Pull-Ansatz können Nachrichten auch außerhalb des Momentes, in dem der Client mit dem Server kommuniziert, empfangen werden.

Wenn man einen normalen E-Mail-Client mit einem normalen E-Mail-Anbieter in den Standardeinstellungen nutzt, dann wird das Protokoll IMAP verwendet, um die E-Mails abzurufen. IMAP ist ein Beispiel für ein Protokoll, in welchem Push-Nachrichten über die selbe Verbindung - insbesondere ohne Zusatzdienstleister - laufen, die auch für das Abrufen der E-Mails (Nachrichtenliste und Nachrichteninhalte) verwendet wird.
<br>
<br>

### 2. Das Ressourcenproblem{#ressourcenproblem}

Es gibt allerdings Plattformen, auf denen viele Clients genutzt werden. Dann kann noch dazu kommen, dass diese Plattformen begrenzte Ressourcen haben. Ein verbreitetes Beispiel dafür sind Smartphones: Neben den (z.T. mehreren) Kommunikationsanwendungen gibt es noch viele Weitere, die die Möglichkeit haben wollen Nutzer in Echtzeit zu erreichen. Bei einigen Anwendungen wie Katastrophenwarnapps ist das auch erforderlich, bei Free-to-Play-Spielen hingegen ist es ein Marketing-Mittel, um Nutzer in die App zu locken.

Um Nachrichten in Echtzeit empfangen zu können müssten die Clients, wie bei den E-Mails, die ganze Zeit aktiv sein und eine Verbindung aufrecht erhalten. Das würde bei vielen Clients Ressourcen verbrauchen. Bei Smartphones ist da z.B. die Ressource Akkulaufzeit sehr kritisch.

#### 2.1 Der Umgang mit diesem Problem{#umgang}

<img src="/img/pushdienste/grafik_2.png" class="fullsize" />
<br>

Einerseits wollen Plattformen genutzt werden (was durch Push-Nachrichten gefördert wird), andererseits wollen diese benutzbar bleiben (d.h. der Smartphoneakku sollte nicht zu schnell leer sein).

Daher gibt es als Lösung Push-Dienste. Auf dem Endgerät ist eine Vermittleranwendung des Push-Dienstes. Die anderen Clients registrieren sich über diese Vermittleranwendung beim Push-Dienst. Der Server der Clients sendet dann Neuigkeiten an den Push-Dienst, der diese über die Vermittleranwendung an die Clients weiterleitet.

Soweit die Theorie. Push-Dienste bringen nur dann etwas, wenn mehrere Anwendungen den selben Push-Dienst benutzen. Plattformen wie Android oder iOS lassen dem Nutzer keine Wahlfreiheit welchen Push-Dienst m / w / d nutzen möchte.
<br>
<br>

### 3. Die üblichen Push-Dienste{#push-dienste}

Im Falle eines handelsüblichen Android Smartphones, mit Google-Apps, ist der übliche und vorinstallierte Push-Dienst "Firebase Cloud Messaging", wobei das Unternehmen Firebase zu Google gehört. Bei Apples mobilem Betriebssystem iOS heißt es "Apple Push Notification Service".

Einige Android-Smartphone-Hersteller haben zusätzlich noch ihren eigenen Push-Dienst. So hat z.B. Samsung den [Samsung Push Service](https://play.google.com/store/apps/details?id=com.sec.spp.push&hl=en_US) und Huawei hat [Huawei Push](https://developer.huawei.com/consumer/en/service/hms/catalog/huaweipush_v3.html?page=hmssdk_huaweipush_introduction_v3).
<br>
<br>

### 4. Die Probleme{#probleme}

#### 4.1 Das Zentralisierungsproblem{#zentralisierung}

Darunter versteht man eine Zentralisierung der Push-Nachrichten beim Server des Push-Dienstleisters.

Dazu kommt, dass Apps ein [SDK](https://anoxinon.media/anxicon/) des Push-Dienst-Anbieters verwenden müssen. Diese SDKs unterstützen folglich dann nur diesen einen Push-Dienst-Anbieter. Da das SDK vom Entwickler integriert wird und nicht vom Benutzer hat man als Nutzer nicht die Möglichkeit einen bestimmten Push-Dienst oder einen eigenen Server zu verwenden.

#### 4.2 Das Datenschutzproblem{#datenschutzproblem}

<img src="/img/pushdienste/grafik_3.png" class="fullsize" />

Wann immer es eine Push-Nachricht über eine solche App gibt, läuft diese Information über den entsprechenden Push-Dienst. Die Anbieter dieser Dienste sitzen meistens in einen anderen Land, in dem der Datenschutz einen anderen Wert hat.

Der Push-Dienst-Anbieter erfährt so mindestens, welche anderen Dienste man nutzt und wann über welchen anderen Dienst eine Nachricht eingeht (die [Metadaten](https://de.wikipedia.org/w/index.php?title=Metadaten&oldid=192222539#Metadaten_bei_der_Kommunikation_im_Internet)). Je nach Client-App wird nur eine Synchronisationsaufforderung, eine Nachricht im Klartext oder eine verschlüsselte Nachricht gesendet.

(Der Server und die Client-App können einen Schlüssel austauschen, den der Push-Dienst-Anbieter nicht bekommt, um die Daten zu "schützen", aber das passiert nicht von selbst.)

Allein die Metadaten sind unverschlüsselte Daten, die sehr viele Details über unser digitales Leben preis geben und die Metadaten kann man nicht verschlüsseln.

#### 4.3 "Besserung in Sicht ?!"{#besserung}

Google selbst bietet beispielsweise eine [fertige Bibliothek](https://github.com/google/capillary) für die Verschlüsselung von Push-Nachrichten über Firebase Cloud Messaging an. Eine feste Integration dieser Funktionalität in das Firebase SDK (der Baustein, den man in eine App einbindet, um dort Firebase Cloud Messaging zu verwenden) gibt es aber nicht, sodass nicht sichergestellt ist, dass dies von den App-Entwicklern verwendet wird.

In Webbrowsern sieht die Lage "besser" aus. Dort [hängt der Push-Dienst vom Browser ab](https://developers.google.com/web/fundamentals/push-notifications/how-push-works#who_and_what_is_the_push_service) und dort ist die Verschlüsselung der Daten der Push-Nachricht [Standard](https://developers.google.com/web/fundamentals/push-notifications/web-push-protocol#the_payload_encryption).
<br>
<br>

### 5. Andere Arten von Push-Diensten - Fazit{#fazit}

Dann gibt es auch noch die Möglichkeit, bei laufenden Clients Nachrichten über Änderungen an den Client zu senden. Dabei kann der Client eine Webseite sein (z.B. eine Cloud ToDo-Liste), eine native App oder auch eine vernetzte Glühbirne. Das sind die Fälle, in denen man eine direkte Verbindung zum Server des Anbieters ohne Probleme benutzen könnte.

Auch für solche Zwecke gibt es externe Dienstleister, die man auch als Push-Dienst bezeichnen kann. Ein Beispiel dafür ist PubNub. Diese haben extra SDKs für Internet of Things Hardware. Eine Liste von Firmen, die PubNub für IoT nutzen, ist [leicht auffindbar](https://www.pubnub.com/customers/?industry=iot-and-smarthome). Auch hier gibt es wieder das Problem der Zentralisierung. Bereits die Steuerung einer vernetzten Lampe kann darüber informieren, ob jemand zu Hause ist. Wenn dann noch der Datenverkehr für ein vernetztes Schloss über den selben Dienstleister läuft wird das eigene Sicherheitslevel stark herab gesetzt. Dafür muss das Türschloss nicht einmal vom selben Hersteller sein, weil es passieren kann, dass beide den selben Diensleister benutzen. Natürlich hat so ein Dienstleister normalerweise keine bösen Absichten. Aber ein Ort, an dem so eine große Ansammlung von "nützlichen" Daten ist, ist immer für Angreifer interessant. Außerdem sitzen diese Dienstleister oft in anderen Ländern mit anderen Gesetzen. Für die Korrektheit muss man natürlich sagen, dass auch, wenn kein Push-Dienst verwendet wird, die Daten oft in anderen Ländern verarbeitet werden. Allerdings steigen die Risiken eines Unfalls und die Menge der gespeicherten Daten im Normalfall mit der Anzahl an Organisationen, die diese Daten verarbeiten.
