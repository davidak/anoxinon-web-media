+++
title = "Linux für Einsteiger - Teil 2"
date = "2019-04-01T16:50:00+02:00"
tags = ["Anfänger"]
categories = ["Linux", "Freie Software"]
banner = "/img/thumbnail/Tux_Installation.png"
description = "Linux, was ist das? In dieser Artikelserie möchten wir interessierte Personen an das Thema Linux heranführen. Weiter geht es mit Teil 2 und der Installation. Dazu servieren wir eine Auswahl an Open Source Software."
+++
>  Dieser Beitrag ist für Anfänger geeignet und richtet sich an Umsteiger von Windows


<h3>I. Einführung</h3>

Nach unserem ersten Teil der "Linux für Einsteiger"-Serie sind Sie ggf. schon auf den Geschmack gekommen und wollen Linux einmal ausprobieren. Doch dann stellt sich die Frage, welche Distribution ist die richtige? Welcher Desktop soll gewählt werden? Worauf muss ich achten? Fragen, die wir in diesem Artikel beantworten wollen. Außerdem geben wir einen Überblick über die Installation und empfehlenswerte Software.

<h3>II. Welche Desktopumgebung?</h3>

Bei Linux gibt es die Qual der Wahl. Fall Sie nur den Standard Desktop von Microsoft oder Apple kennen, ist die Entscheidung vielleicht nicht so einfach.

Meist gibt es die Auswahl zwischen den Desktopumgebungen Gnome, KDE, xfce und Cinnamon - wobei Gnome und KDE sehr weit verbreitet sind. Die Entscheidung, welchen Desktop man wählt, ist reine Geschmackssache. Während Gnome eher minimalistisch ist und mit seinem Design vielleicht an Android oder iOS erinnert, gibt es bei KDE unzählige Einstellungsmöglichkeiten. KDE punktet aber auch mit einer größeren Anpassbarkeit der einzelnen Widgets und Fenster und kann somit individueller gestaltet werden.

Nachfolgend ein kleines Beispiel, wie unterschiedlich ein System mit verschiedenen Desktopumgebungen aussehen kann:
<hr>
  <img src="/img/linuxfuereinsteiger/LMCinnamon.png" width="783" height="385" alt="Cinnamon" class="fullsize">
  <br>
Distribution: Linux Mint - Desktopumgebung: Cinnamon<br>
Hat am ehesten noch einen „Windows“-Look. Vertraute Startleiste links unten mit übersichtlicher Programmauflistung.
<hr>
  <img src="/img/linuxfuereinsteiger/MJGnome.png" width="783" height="385" alt="Cinnamon" class="fullsize">
<br>
Distribution: Manjaro -
Desktopumgebung: GNOME
<hr>
  <img src="/img/linuxfuereinsteiger/MJKDE.png" width="783" height="385" alt="Cinnamon" class="fullsize">
<br>
Distribution: Manjaro -
Desktopumgebung: KDE
<hr>
Sie sollten sich vor der Installation auf eine Variante festlegen.<br>
Übrigens: Gnome ist momentan nicht für Linux Mint erhältlich. Hier gibt es nur Cinnamon oder KDE.

<h3> III. Vorbereitung</h3>

Bevor Sie mit der Installation beginnen, sichern Sie alle Ihre Daten und erstellen ausreichend Backups! Bitte beachten Sie, dass wir keine Haftung für die Vollständigkeit und Richtigkeit unserer Anleitung übernehmen.

**Schritt 1:** Zuerst müssen Sie wissen, ob Sie ein 32-Bit- oder 64-Bit-System haben. Sollten Sie einen Windows-PC nutzen, finden Sie die Info, welches System Sie nutzen, unter:

Windows-Button/Start-Button
→ System und Sicherheit → System → Hier befindet sich die Info, ob die Windows-Version 32 Bit oder 64 Bit unterstützt.

**Schritt 2:** Besuchen Sie die Website der Distribution Ihrer Wahl.

* Für Mint: https://linuxmint.com/

* Für Fedora: https://getfedora.org/de/

* Für MX: https://mxlinux.org/

Je nach System wählen Sie jetzt entweder die 32-Bit- oder die 64-Bit-Version.

Natürlich gibt es viel mehr Distributionen (wie SUSE, Debian usw.), jedoch empfinden wir sie nicht als sehr anfängerfreundlich, weswegen wir unsere Empfehlung auf Mint, Fedora und MX beschränkt haben.

**Schritt 3:** Sie laden nun das Image (die .iso) herunter und warten bis der Download vollständig abgeschlossen ist.

**Schritt 4:**
Sie müssen das Image nun auf den USB-Stick schreiben. Hier gibt es zum Beispiel das Programm [Rufus](https://rufus.ie/). Es ist auch möglich, eine DVD zu verwenden. Wir empfehlen aber die Verwendung eines USB-Sticks.


<h3>IV. Linux installieren</h3>

Nach der Erstellung des USB Sticks möchte man natürlich so schnell wie möglich das neue Betriebssystem testen. Einfach eine DVD oder den USB-Stick einstecken, wird nicht funktionieren. Der PC/Laptop muss ausgeschaltet bzw. neu gestartet werden und ggf. ein paar Anpassungen vorgenommen werden.

**Schritt 1:** Deaktivieren Sie den Windows Schnellstart in Systemsteuerung → Energieoptionen → "Auswählen, was beim Drücken des Netzschalters geschehen soll" →  Auf den Satz "Einige Einstellungen sind momentan nicht verfügbar" klicken und den Vorgang mit Administratorrechten genehmigen → Einstellungen fürs Herunterfahren → Haken bei "Schnellstart aktivieren" entfernen. Im Anschluss starten Sie Ihren PC neu.

**Schritt 2:** F-Tasten (F1 bis F12) schnell durchprobieren, um in den UEFI- oder BIOS-Modus zu gelangen. Leider können wir keine genaue Taste nennen, da die Belegung herstellerabhängig ist. F1, F2 oder die ENTF-Taste sind beliebte und gängige Varianten.

Ihnen kann auch eine [Startpage](https://www.startpage.com) Suche (oder andere Suchmaschine Ihrer Wahl) helfen. Suchen Sie nach „Name des Laptop Models + BIOS“.

**Schritt 3:**  Falls Sie noch nie im Bootmenü waren: Sie navigieren mit den Pfeiltasten und "**Enter**"! Ihre Maus wird hier nicht funktionieren. Als kleine Erinnerung, wie Sie wohin navigieren können, werden die Tasten ganz unten noch einmal aufgelistet.

Suchen Sie in den Reitern "Boot, Security oder Authentication" nach der Option "Secure Boot" und stellen sie diese auf disabled.

Navigieren Sie nun zu dem Reiter "Boot" und setzen den USB-Stick bzw. die DVD an erste Stelle. Sie wählen mit den Pfeiltasten den Eintrag aus und können mit +/- die Reihenfolge ändern. Drücken Sie im Anschluss F10 und speichern Sie die Änderungen.

**Schritt 4:** Nach dem Neustart haben Sie meist eine Auswahl zwischen direkter Installation, Live-Modus und OEM-Installation. Wählen Sie den Live-Modus mit den Pfeiltasten und bestätigen Sie mit Enter.
<hr>

Sie befinden sich nun in einem LIVE-Test-Modus, das heißt, Sie können das System etwas ausprobieren. Das System kann hier langsam reagieren und auch, je nach Leistungsstärke des Rechners, ruckeln. Das ist vollkommen normal, da die Lese- und Schreibvorgänge via USB in der Regel langsamer sind als von der Festplatte. Wenn Ihnen das System gefällt, kann der Installationsprozess gestartet werden. Hierfür gibt es eine Verknüpfung auf dem Desktop, die Install-Mint/MX/Fedora heißt. Klicken Sie zweimal schnell hintereinander auf diese und die Installation beginnt.

Sollten Sie Hilfe bei der Installation benötigen, schauen Sie doch einmal in das von uns erstellte PDF mit weiteren Empfehlungen.

<div style="text-align: center">  <a href="/img/linuxfuereinsteiger/Mint_Installation.pdf" class="btn btn-small btn-template-main">Download Anleitung</a></div><br>

Die Installation unterscheidet sich zu anderen Distributionen nur minimal.
<hr>
Das System können Sie nun nach Ihren Wünschen einrichten, wir empfehlen:

* Eine Verschlüsselung mit sicherem Passwort und ohne Umlaute! Hinweise zu sicheren Passwörtern gibt es [hier](https://www.kuketz-blog.de/passphrase-sensible-daten-via-diceware-schuetzen/). Nutzen Sie, wenn möglich, die Minimalinstallation und verzichten Sie auf unfreie Software.

* Falls Sie sich von der grafischen Oberfläche wegbewegen wollen, haben wir noch zwei Übersichtsblätter für die ersten Befehle gefunden: [Linux auf einem Blatt](http://helmbold.de/artikel/Linux-auf-einem-Blatt.pdf) und [die Shellübersicht](http://www.321tux.de/wp-content/uploads/2010/03/shell-uebersicht.pdf).

<h3> IV. Software Empfehlungen</h3>

Nach der Erstinstallation stellt sich meist die Frage, welche Software man installieren kann. Hierfür möchten wir Ihnen eine Empfehlungsliste für Open-Source-Software an die Hand geben.

**Bildbearbeitung**

* Photoshop-ähnlich: [Gimp](https://www.gimp.org), [Krita](http://www.krita.org)
* Vektorgrafiken: [Inkscape](http://www.inkscape.org)
* Bildverwaltung: [Shotwell](https://wiki.ubuntuusers.de/Shotwell/)
* 3D-Animationen: [Blender](http://www.blender.org)
* Flyer: [Scribus](http://www.scribus.net)
* Lightroom-ähnlich: [Darktable](https://www.darktable.org)
* RAW-Fotos: [RAW-Therapee](http://www.rawtherapee.com)
* Panorama: [Hugin](https://wiki.ubuntuusers.de/Hugin/)

**Rund ums Büro**

* Office: [LibreOffice](https://www.libreoffice.org)
* PDF Reader: [Okular](https://okular.kde.org/), [Evince](https://wiki.gnome.org/Apps/Evince)
* Mindmaps: [Freeplane](https://www.freeplane.org/wiki/index.php/Home)
* Scanprogramm: [SimpleScan](https://wiki.ubuntuusers.de/Simple_Scan/)
* Notizen: [Rednotebook](https://wiki.ubuntuusers.de/RedNotebook/), [Tasque](https://wiki.gnome.org/Attic/Tasque), [Aikee](https://github.com/rockiger/akiee)
* Karteikarten: [Anki](https://apps.ankiweb.net/)
* Pack-Programme: [XArchiver](https://en.wikipedia.org/wiki/Xarchiver), [Ark](https://kde.org/applications/utilities/ark/)
* CDs/DVDs brennen: [k3B](https://wiki.ubuntuusers.de/K3b/), [Brasero](https://wiki.gnome.org/Apps/Brasero)
* Banking/Finanzen: [Hibiscus](https://www.willuhn.de/products/hibiscus/)

**Tools**

* Bildschirmfotos: [Shutter](http://), [Kazam](https://launchpad.net/kazam), [Flameshot](https://flameshot.js.org/), [Grim](https://github.com/emersion/grim)
* ebook Reader: [Calibre](http://www.calibre-ebook.com)
* Make Startup Disk: [USB Sticks mit .iso Bootimage erstellen](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-ubuntu)
* Passwort-Manager: [KeePassXC](http://www.keepassxc.org)
* Nextcloud Client: [Nextcloud](https://nextcloud.com/clients/)
* Remote Desktop: [Remmina](http://www.remmina.org)
* Virtuelle Maschine: [VirtualBox](http://www.virtualbox.org)
* Verschlüsselung: [VeraCrypt](http://www.veracrypt.fr)
* Desktop Automation Utility: [Autokey](http://https://github.com/autokey/autokey)
* Textvergleich: [Meld](http://www.meldmerge.org)

**Kommunikation**

* Email Client: [Thunderbird](https://www.thunderbird.net/)
* XMPP Messenger Client: [Gajim](http://www.gajim.org), [Dino](https://dino.im)
* Matrix Client: [Riot](https://www.riot.im)
* Voice Chat: [Mumble](http://www.mumble.info)

**Web:**

* FTP: [FilleZilla](https://filezilla-project.org/download.php)
* Browser: [Firefox](https://www.mozilla.org/en-US/firefox/new/)

**Video & Audio**

* Videoplayer: [VLC](http://www.videolan.org), [Smplayer](http://www.smplayer.info),
* Audioplayer: [VLC](http://www.videolan.org), [Banshee](http://www.banshee.fm), [Amarok](http://amarok.kde.org)
* Audiokonverter: [Asunder](http://https://wiki.ubuntuusers.de/Asunder/)
* Videoaufzeichnung/Live Streaming: [OBS Studio](https://obsproject.com/), [SimpleScreenRecorder](https://www.maartenbaert.be/simplescreenrecorder/)
* Videobearbeitung: [Openshot](http://www.openshot.org), [Kdenlive](http://www.kdenlive.org)
* Audiobearbeitung: [Audacity](http://www.audacityteam.org)

**Backups**

* Systembackup: [Timeshift](https://github.com/teejee2008/timeshift)
* Komplette Backups: [Clonezilla](https://clonezilla.org/)
* Backuplösung: [rsync](https://wiki.ubuntuusers.de/rsync/), [luckybackup](http://http://luckybackup.sourceforge.net/), [Restic](https://github.com/restic/restic)
* File Synchronization: [Syncthing](http://www.syncthing.net)


**Softwareentwickler:**

* IDE: [Intellji](http://www.jetbrains.com/intellji), [Eclipse](http://www.eclipse.org) (Java), [Pycharm](https://www.jetbrains.com/pycharm/) (Python), [Atom](https://github.com/atom), [Visual Studio Code](http://code.visualstudio.com), [Emacs](https://www.gnu.org/s/emacs/)
* Markdown Editor: [TYPORA](http://www.typora.io)

**Sie brauchen weitere Ideen?**

* [Archlinux Wiki](https://wiki.archlinux.org/index.php/list_of_applications)
* [Ubuntuusers.de](https://ubuntuusers.de/)

<h3>V. Das Wort zum Schluss</h3>

Bedenken Sie, Linux ist nicht Windows oder MacOS. Linux geht seinen eigenen Weg und unterscheidet sich von Ihnen bekannten Betriebssystemen. Sie können bei Problemen eine große Community um Rat bitten. Es gibt zahlreiche Chaträume, Foren oder Hilfeseiten. (Bspw. [Ubuntuusers.de](https://ubuntuusers.de/)) Sollten Fehler auftreten oder Sie nicht mehr weiter wissen, geben Sie nicht frustriert auf, sondern suchen Sie nach Rat. :)

Empfehlenswert sind auch regionale/lokale Treffen von sogenannten "Linux User Groups" o.ä.<br>
Hier können Sie ebenso Hilfe bei der Installation und Einrichtung erhalten.

Vielen Dank fürs Lesen!

> Lizenzhinweis zum Thumbnail: Der Tux Pinguin wurde erstellt von Larry Ewing (lewing@isc.tamu.edu) and The GIMP
