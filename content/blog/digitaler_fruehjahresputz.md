+++
title = "Digitaler Frühjahrsputz"
date = "2019-03-06T18:30:00+02:00"
tags = ["Anfänger"]
categories = ["Allgemeines"]
banner = "/img/thumbnail/Thumbnail_digitaler_Abfall.png"
description = "Der Frühling kommt und einige misten ihre Wohnung aus. Haben Sie sich schon einmal überlegt, wieviel Müll sich in Ihrem digitalen Leben angesammelt hat?"

+++

### I. Digitale Ordnung ###

Es ist Frühling und es wird langsam Zeit, wieder einmal aufzuräumen. Nicht nur im Haushalt sammelt sich vieles an, auch der Rechner quillt über. Der Desktop ist voller abgelegter Dateien, die irgendwann einmal dort platziert worden sind. Das letzte Backup ist auch schon etwas her und wann wurden zuletzt die E-Mails archiviert bzw. aussortiert?

So toll die Digitalisierung manchmal ist, so viele Schattenseiten hat sie auch:
<ul>
  <li>Wir sind weniger aufnahmefähig und leicht ablenkbar</li>
  <li>Wir fühlen uns aus Angst etwas zu verpassen und die "Pflicht" schnell antworten zu müssen, unnötig gestresst</li>
  <li>Wir haben zu viele Konten, die ständigen Sicherheitsrisiken der Hersteller ausgeliefert sind</li>
  <li>Wir tauchen manchmal zu tief ein und vergessen auf anderes.</li>
  <li>Wir präferieren immer mehr Quantität vor Qualität.
</ul>

Wir sollten uns endlich mal wieder etwas "freier" fühlen. Ich möchte mich nicht alle 2 Wochen irgendwo "einloggen" müssen, weil der nächste Datengau bekannt wurde. Ich möchte nicht von Newslettern überflutet werden oder dass Adresshändler meine Daten verkaufen, um mir noch mehr Werbung zu schicken. Für alle, die sich von diesem Stress oder Müll befreien möchten, ist dieser Bonusartikel gedacht.


### II. Am PC ###

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 1: Festplatte aufräumen</b>
Veraltete und unstruktierte Dateien, die nicht mehr benötigt werden, endlich löschen. Geben Sie den Daten auf Ihrer Festplatte eine Struktur und denken Sie daran, diese auch [verschlüsselt](https://www.veracrypt.fr/en/Downloads.html) abzulegen. Sie haben endlich wieder die Möglichkeit, sich auf das Wesentliche zu konzentrieren und können effektiver und mit weniger Ablenkung arbeiten.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 2: Backup</b>
Was man noch aufheben möchte oder gar muss, sollte man entsprechend sichern. Am besten auf eine externe (verschlüsselte) Festplatte. Eine gute Backupmöglichkeit bietet euch das Tool "[rsync](https://wiki.ubuntuusers.de/rsync/)".

Wer sich ohne grafische Benutzeroberfläche unwohl fühlt, für den gibt es [luckybackup](https://wiki.ubuntuusers.de/luckyBackup/) oder [Freefilesync](https://freefilesync.org/).

Kleiner Hinweis zu vorverschlüsselten Festplatten: Derzeit gibt es Festplatten im Handel, die "von Haus aus" verschlüsselt sind. Der Code hierfür ist nicht bekannt, auch ist unklar, wer ggf. diese Platten wieder entschlüsseln kann. Daher raten wir, Platten immer selbst zu verschlüsseln (z. B. mit [Veracrypt](http://www.veracrypt.fr)) und sich darum zu kümmern.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30"  /> <b>Schritt 3: Linux ausprobieren</b>
Falls wir Sie mit unserem Linuxbeitrag überzeugen konnten und Sie diesen Schritt [nach unserem Artikel](https://anoxinon.media/blog/linuxfuereinsteiger1/) noch nicht erledigt haben: Ihr PC läuft potenziell schneller und kommmt von Haus aus ohne vorinstallierte "Werbe Apps".

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 4: Systempflege</b>
Das Update wurde wieder zulange herausgezögert. Jetzt ist endlich Zeit dafür.
Nicht benötigte Programme bzw. Pakete sollten gelöscht werden.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 5: Unnütze Konten</b>
Konten, die Sie sowieso nicht brauchen bzw. nutzen, können endlich gelöscht werden.
Sie reduzieren das Risiko, diese komplett zu vergessen. Wie viele der angemeldeten Konten haben Sie in den letzten 365 Tagen aktiv genutzt? Es gibt zahlreiche Seiten, die es Ihnen sogar einfach machen und eine direkte Anleitung bereitstellen, wo sich der Löschlink befindet, z.B. www.justdelete.me.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 6: Werbeblocker aktivieren</b>
Holen Sie sich [ublock origin](https://addons.mozilla.org/de/android/addon/ublock-origin/) für Ihren Browser und blocken Sie damit agressive Werbung. Die Seiten werden schneller geladen und Sie werden nicht durch nervige Werbe Pop-Ups gestört.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 7: Der Posteingang</b>
Schon von Anbietern wie Gmail oder Hotmail weg gezogen?
Gut, dann sammeln Sie trotzdem nicht E-Mails, weil der Speicherplatz vorhanden ist. Sie werden die meisten E-Mails nicht zweimal lesen, daher löschen Sie Überflüssiges. Das ganze hilft auch präventiv, falls jemand ungefragt Ihr Mailkonto übernimmt. (Die PGP-Verschlüsselung könnte auch vorbeugen, die ist aber leider nicht bei jedem vorhanden)

Interessiert Sie der Inhalt Ihrer E-Mail-Newsletter überhaupt? Nehmen Sie sich die Zeit diese abzubestellen.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b> Schritt 8: Werbewidersprüche</b>
Die Landesbeauftragte für den Datenschutz und Informationsfreiheit Rheinland-Pfalz bietet ein nahezu fertiges [Formular](https://www.datenschutz.rlp.de/fileadmin/lfdi/Dokumente/Orientierungshilfen/muster_werbewiderspruch.rtf ). Entweder copy&paste per E-Mail oder postalisch an die entsprechenden Unternehmen schicken und bestätigen lassen. Dabei sollten Sie nicht nur an die Versicherungen/Verträge denken, die Sie aktiv abgeschlossen haben, sondern auch an bekannte Adresshändler.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b> Schritt 9: Übersicht eurer Konten und Passwortmanagement</b>
Welche Konten nutzen Sie überhaupt? Machen Sie sich am besten eine Liste (vorzugsweise via [Keepass](https://keepassxc.org/) und nicht auf Papier), auf welcher Seite sind Sie mit welcher E-Mail-Adresse registriert - Nickname, Kundennummer und vor allem das Passwort. Man sollte das gleich als Anlass nehmen, die Passwort Gewohnheiten zu ändern und diese sicher zu gestalten. Wir empfehlen hierzu diese weiterführende [Lektüre](https://www.kuketz-blog.de/sicheres-passwort-waehlen-der-zufall-entscheidet/).

### III. Auf dem Smartphone ###

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 1: Bilder auf dem Smartphone löschen</b>
Auch wenn man sein Smartphone nicht dauerhaft nutzt, sammeln sich Fotos an. Auch hier sollte man häufig ausmisten. Wenn man etwas behalten möchte, sollte man diese Daten anderweitig abspeichern, zum Beispiel auf einer externen Festplatte. Sie brauchen nicht alle Fotos ständig bei sich.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 2: Adressbuch aussortieren & Karteileichen löschen</b>
Viele Apps greifen auf das Adressbuch zu, ob sie die Berechtigung benötigen oder nicht. Dieses Verhalten sollte man generell unterbinden. Sollte es dennoch zu einem Fehler kommen, kann es sein dass das gesamte Adressbuch weiterverkauft wird. Löschen Sie daher Kontakte, die Sie nicht mehr benötigen oder einfach Datenleichen sind. In diesem Zusammenhang lohnt sich auch ein Blick auf die App [Open Contacts](https://f-droid.org/app/opencontacts.open.com.opencontacts), welche Ihr Adressbuch in einer anderen Datenbank speichert, als andere Adressbuch-Apps. Die Daten sind somit vom Android-Adressbuch getrennt und andere Apps können nicht mehr so einfach auf Ihre Kontakte zugreifen.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 3: Prüft, ob eure Web/App-Deaktivierungen noch aktiv sind und die Firewall noch funktioniert</b>
Sollten Sie keine Firewall auf Ihrem Gerät haben, gibt es für gerootete Geräte [AFWall+](https://www.kuketz-blog.de/afwall-wie-ich-persoenlich-die-android-firewall-nutze/). Für normale Geräte ist zumindest [Blokada](https://www.kuketz-blog.de/blokada-tracking-und-werbung-unter-android-unterbinden/) eine gute Lösung.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 4: Sollten Sie immer noch Google nutzen, jetzt ist die Zeit, sich auch hiervon zu trennen
;)</b>
Fangen Sie klein an: Ersetzen Sie die Suchmaschine durch eine Alternative wie Searx oder [Startpage](https://www.startpage.com), ersetzt Google Maps mit [OpenStreetMaps](http://www.openstreetmaps.org) oder euer Google Keyboard mit [Anysoftkeyboard](https://f-droid.org/en/packages/com.anysoftkeyboard.languagepack.german/).

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> <b>Schritt 5: Wie viele Apps brauchen Sie wirklich?</b>
Behalten Sie nur die Apps, die Sie regelmäßig benötigen. Falls Sie die Apps doch noch einmal brauchen sollten, sind diese schnell wieder installiert.
Übrigens: Falls Sie wissen wollen, wie datenschutzfreundlich eine App ist, gibt es [Exodus Privacy](http://exodus-privacy.eu.org/en/). Generell raten wir jedoch, den [F-Droid Store](https://www.f-droid.org) zu nutzen, der fast ausschließlich OpenSource-Apps beinhaltet.

### IV. Social Networking ###
Folgende Frühjahresputztipps kommen von unserer Mastodon-Community. Danke für die Beteiligung an dieser Stelle.<p>

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> **Schritt 1: Chatverläufe löschen**
  Müssen die Chatverläufe der letzten Jahre wirklich dauerhaft gespeichert sein oder sogar auf neue Geräte importiert werden? Befreien Sie Ihr Smartphone daher regelmäßig von überflüssigen Nachrichten, die kein zweites Mal gelesen werden. Dies betrifft natürlich auch Sprachnachrichten.

<img src="/img/thumbnail/ICON_digitaler_Abfall.png" align="middle" height="30" width="30" /> **Schritt 2: Sind Ihre Onlinebeiträge noch aktuell?**
Nichts ist so schnell out wie Nachrichten aus dem Netz. Was heute noch topaktuell ist, ist morgen schon ein alter Hut. Wenn Sie möchten, können Sie diese entfernen. Es gibt natürlich keine Garantie dass alles unwiderruflich aus dem Internet verschwindet. Für Mastodon gibt es sogar [ein Skript](https://github.com/ThomasLeister/mastopurge).
<br>


### V. An alle Webmaster ###

Es ist nicht unwahrscheinlich, dass auch Webmaster, Blogger o.ä. unseren Beitrag gerade lesen. Sie brauchen normalerweise keinen Datenberg mit Kunden- und Interessenteninformationen, um erfolgreich zu sein. Viele Schritte, die oben bereits genannt wurden, lassen sich auch von Ihnen anwenden. Ein paar Zusatzinformationen kann man Ihnen dennoch an die Hand geben. Minimiert das Tracking. Es werden oft einfach Tracker eingebunden, ohne dass diese aktiv ausgewertet werden. Die Auswertung bedeutet zusätzlichen Aufwand und mit den meisten Analysetools oder Widgets verschenken Sie sogar noch wichtige Informationen über Ihre Seite bzw. Kunden an Google, Facebook & Co. Sollten Sie Datenbanken (Newsletter, Kundenkonten etc.) führen, dann löschen Sie diese in regelmäßigen Abständen. Als Beispiel nehmen wir einmal das Kundenkonto: Wenn Ihre Kunden sich für einen bestimmten Zeitraum nicht eingeloggt haben, wird der Account wahrscheinlich nicht mehr aktiv genutzt oder die Zugangsdaten sind abhanden gekommen. Eine Löschung würde nicht nur den besseren Schutz der Kunden bedeuten, sondern auch gleichzeitg weniger Ressourchenverbrauch. Zu guter letzt, aktualisieren Sie regelmäßig die Systeme und halten Sie diese up2date.
Behalten Sie nur, was auf Grund von z. B. finanzrechtlichen oder anderen rechtlichen Pflichten benötigt wird!

### VI. Schlussworte ###

Im Internet gibt es viele fadenscheinige Empfehlungen, wie Sie die digitale Ordnung aufrecht erhalten. Darunter befinden solche, wie die alten Familienfotos in Dropbox, Google Cloud oder iCloud zu laden und die Familienplanung über Evernote zu organisieren. Die Fotos der Liebsten haben in einer Cloud, ohne Zustimmung der Beteiligten nichts verloren. Selbstgehoste Dienste wie Nextcloud oder Anbieter, die auf offene Standards setzen, sind zu bevorzugen.

Wenn man schon digitalisieren möchte, dann geht das auch ohne die benannten Cloud Lösungen. Im Fall der Fälle kann man einzelne Fotos weiterschicken oder ausdrucken. Alternativ ist es auch möglich, ganz analog in Erinnerungen zu schwelgen und die Erlebnisse zu teilen. Die Cloud ist am Ende nur der Computer eines unbekannten Dritten und dem muss zusätzlich vertraut werden.

Wir verkomplizieren unnötig unser Leben mit immer neuen digitalen Zeitfressern. Folgen Sie lieber dem Trend des "Digital Detox" und prüfen Sie Ihre Geräte nicht ständig auf neue Nachrichten und E-Mails. :)
