+++
title = "Empfohlene Dienste"
description = "Empfohlene Diensten"
keywords = ["extras","Downloads","Links","Anxicon", "Dienste"]


+++
Nachfolgend eine Auflistung von Diensten, je nach Kategorie.


---

**E-Mail Anbieter**  

- [mailbox.org](https://mailbox.org) - E-Mail Anbieter  
- [tutanota.com](https://tutanota.com/) - E-Mail Anbieter (mit Verifizierung über XMPP)  

E-Mail Anbieter, die mit "[Email made in Germany](https://media.ccc.de/v/30C3_-_5210_-_de_-_saal_g_-_201312282030_-_bullshit_made_in_germany_-_linus_neumann)" (Gmx, Web.de, Freemail etc. ) werben, zählen für uns **nicht** zu den Pionieren in Sachen Datenschutz. <br>
Alternativ kann man Sich einen eigenen E-mail Server aufsetzen oder z.B. eine [Mailcow](https://mailcow.email/).

---

**Suchmaschinen**   

- [startpage.com](https://startpage.com) - niederländische Suchmaschine, die Ergebnisse von Google bekommt und dazwischen als Proxy agiert, heißt Google bekommt keine IP oder ähnliches.  
- [searx.me](https://searx.me) - Open Source Suchmaschine  
- [metager.de](https://metager.de) - Angebot des SUMA-EV, sogar mit Proxy-Seiten-Aufruf. Metager ist zudem direkt aus dem Tor Netzwerk erreichbar
- [qwant.com](https://qwant.com) - französische Suchmaschine mit umfassendem Suchangebot, wie zB.: [Kartendienst](https://qwant.com/maps) und eine [Kindersuchmaschine](https://www.qwantjunior.com/)

Oft werden noch DuckDuckGo und Ecosia genannt, welche wir jedoch nicht in das Ranking aufnehmen. DuckDuckGo hat seinen Standort in den USA und Ecosia, wirbt zwar mit Spenden für die Umwelt nutzt aber viele Facebook Tracker.

---

**Navigation**  

OpenStreetMaps gibt es für sehr viele individuelle Möglichkeiten z. B. Rollstuhlfahrer, um einen Briefkasten, eine Bushaltestelle zu finden, Briefkästen, Wanderwege,  Fahrradwege und ganz klassisch um mit dem Auto oder zu Fuß schnell von A nach B zu kommen.   

- [openstreetmap.de](https://openstreetmap.de) - deutsches OpenStreetMap Projekt  
- [openstreetmaps.org](https://openstreetmaps.org) - OpenStreetMap Projekt  

---

**Messenger**

Hier bietet sich das XMPP Protokoll an. Registrieren kann man sich bspw. unter:   

- [anoxinon.de](https://anoxinon.de)  
- [trashserver.net](https://trashserver.net)  
- [wiuwiu.de](https://wiuwiu.de)  
- [tchncs.de](https://tchncs.de)  

Es gibt natürlich viele weitere XMPP Server. Eine gute Übersicht bietet die [Compliance Liste](https://compliance.conversations.im/) von Conversations.

XMPP-Clients gibt es für Windows ([Gajim](https://gajim.org)), Linux (Dino, [Gajim](https://gajim.org)),  Android ([Conversations](https://f-droid.org/en/packages/eu.siacs.conversations), [Pix-Art-Messenger](https://f-droid.org/en/packages/de.pixart.messenger)) sowie iOS (Monal). Und wer mit XMPP noch alleine ist, kann zumindest einen der zahlreichen MUC (Multi-User-Chats) beitreten. Es gibt sogar eine [Conversations Anleitung](/files/Anleitung_Conversations.pdf).

---

**Terminvereinbarung**  

- [dudle.inf.tu-dresden.de](https://dudle.inf.tu-dresden.de/) - Doodle Umfragen starten  
- [systemli.org](https://systemli.org/poll/) - Croodle Umfragen erstellen  

Eine Anleitung zur Nutzung findet man [hier](https://fsi.spline.de/wiki/index.php/Dudle_erstellen).
**Bitte beachten, dass man niemals den vollen Namen eingeben sollte, egal wie gut der Datenschutz ist.**  

Alternativ kann man, sofern man eine Nextcloud Instanz besitzt, die App "Polls" verwenden.

---

**Online-Telefonie / VOIP**  

[Mumble](https://mumble.info) ist eine Sprachkonferenzsoftware. In privaten Räumen kann man sich austauschen. Mumble kann entwder selbst gehostet werden oder man sucht sich einen Server seines Vertrauens. Wer sich bei Mumble unterhalten möchte, muss sich auf denselben Server befinden, da man sich in Räumen trifft.  

Mumble Server:

* [Anoxinon](https://anoxinon.de/dienste/mumble/)  
* [Natenom](https://natenom.com/mymumbleserver)  
* hier [eine Übersicht aller öffentlicher Mumble Server](https://mumble.com/serverlist/?_set[language]=da)  

Mumble-Clients gibt es für Windows, Linux, Mac und Android.

---

**VPN Anbieter**  

Grundsätzlich gilt: Anonymitätsversprechen von VPN Betreibern sollte man kein Vertrauen schenken.<br>
Ein VPN kann jedoch in öffentlichen bzw. nicht vertrauenswürdigen WIFi Netzen oder im Ausland sehr nützlich sein.<br>
Durch einen VPN erhält man keine Anonymität. (Weitere Ausführungen folgen.)


---

**Arbeiten im Team**  

Um im Team gemeinsam Texte zu verfassen eignet sich Cryptpad.

- [cryptpad.fr](https://cryptpad.fr) - Cryptpad

Alternativ kann man Cryptpad selber hosten oder eine andere Instanz suchen.

Bitte beachten, dass man keine sensiblen Daten in Pads speichern sollte, da diese auch ggf. anderen zugänglich gemacht werden könnten und was dort einmal eingetragen wurde bleibt dort (History-Funktion).

---

**Cloud bzw. Onlinespeicher**  

Bei der Erwähnung von Cloud fällt vielen Gesprächspartnern, automatisch Namen wie "iCloud", "Dropbox" & "Google Cloud" ein.
Es gibt genug Gründe, die gegen diese Anbieter sprechen. Die Digitalcourage hat hier schon [einen perfekten Beitrag](https://digitalcourage.de/digitale-selbstverteidigung/alternativen-zu-dropbox-und-cloud) geschrieben.

Am besten hostet man seine Cloud selbst, z. B. mit [Nextcloud](https://nextcloud.com) (offener Quellcode). <br>
Man sollte seinen Daten zusätzlich verschlüsseln z. B. mit [cryptomator](https://cryptomator.org/) oder die Ende-zu-Ende Verschlüsselung von Nextcloud verwenden.

Alternativ schaut man sich im Internet nach Nextcloud Instanzen um oder fragt Bekannten/Freunde mit mehr technischem Wissen.

---

**Github Alternative**  

Wo seinen Code hosten, seit der Übernahme von Github durch Microsoft?

- [codeberg.org](https://codeberg.org) - ein Git-Host Angebot des Vereins Codeberg e.V.
- [tchncs.de](https://git.tchncs.de/users/sign_in) - Das Gitea Angebot von tchncs  

Alternativ gibt es die Möglichkeit Gitea oder Gitlab, selber zu hosten bzw. anderen öffentlichen Instanzen beizutreten.

---

**Youtube Alternative / Videos**  

- [invidio.us](https://invidio.us) - Spiegelung von Youtube-Videos, Youtube erhält nur noch die IP  
- [Peertube](https://joinpeertube.org) - Förderierte Video Sharing Plattform

---

**Fediverse | ein dezentrales soziales Netzwerk**

Einen guten Überblick über das Fediverse liefert [unser Artikel](https://anoxinon.media/blog/fediversewasistdas/).  

Mastodon  

*  [social.anoxinon.de](https://social.anoxinon.de)  
*  [mastodon.at](https://mastodon.at)  
*  [metalhead.club](https://metalhead.club)  
*  [social.tchncs.de](https://social.tchncs.de)  
*  [chaos.social](https://chaos.social.de)
*  [bonn.social](https://bonn.social)  

Weitere Instanzen findet man [in dieser Liste](https://instances.social/).

---

**Websiten auf Datenschutz prüfen**  

- [webbkoll.dataskydd.net](https://webbkoll.dataskydd.net) - prüft Websiten auf Datenschutz, speziell auf Tracker  
- [privacyscore.org](https://privacyscore.org) - prüft Websiten auf Datenschutz (deutsches Projekt)  


---

**Alles Andere**  

- [framaforms.org](https://framaforms.org) - Datenschutzfreundliche Umfragen (Achtung, Französisch)  
- [frama.link](https://frama.link) - Link Shortener (Achtung, Französisch)  

---
