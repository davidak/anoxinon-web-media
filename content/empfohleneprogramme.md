+++
title = "Empfohlene Programme"
description = "Empfohlene Programme"
keywords = ["extras","Downloads","Links","Anxicon", "Programme"]


+++

Nachfolgend eine Auflistung von Programmen, je nach Betriebssystem. (PC/Mobil)

---

### Linux ###

Wie man Linux installiert, haben wir [hier](https://anoxinon.media/blog/linuxfuereinsteiger2/) beschrieben.

---


**Bildbearbeitung**

Photoshop-ähnlich: [Gimp](https://gimp.org), [Krita](https://krita.org)  
Vektorgrafiken: [Inkscape](https://inkscape.org)  
Bildverwaltung: [Shotwell](https://wiki.ubuntuusers.de/Shotwell)  
3D-Animationen: [Blender](https://blender.org)  
Flyer: [Scribus](https://scribus.net)  
Lightroom-ähnlich: [Darktable](https://darktable.org)  
RAW-Fotos: [RAW-Therapee](https://rawtherapee.com/)  
Panorama: [Hugin](https://wiki.ubuntuusers.de/Hugin)  

---

**Rund ums Büro**

Office: [LibreOffice](https://libreoffice.org/LibreOffice)  
PDF-Reader: [Okular](https://okular.kde.org/Okular), [Evince](https://wiki.gnome.org/Apps/Evince)  
Mindmaps: [Freeplane](https://freeplane.org/wiki/index.php/HomeFreeplane)  
Scanprogramm: [SimpleScan](https://wiki.ubuntuusers.de/Simple_Scan/SimpleScan)  
Notizen: [RedNotebook](https://wiki.ubuntuusers.de/RedNotebook/Rednotebook), [Tasque](https://wiki.gnome.org/Attic/TasqueTasque), [Aikee](https://github.com/rockiger/akiee)  
Karteikarten: [Anki](https://apps.ankiweb.net/Anki)  
Pack-Programme: [Xarchiver](https://en.wikipedia.org/wiki/XarchiverXArchiver), [Ark](https://kde.org/applications/utilities/ark)  
CDs/DVDs brennen: [K3B](https://wiki.ubuntuusers.de/K3b), [Brasero](https://wiki.gnome.org/Apps/Brasero)  
Banking/Finanzen: [Hibiscus](https://willuhn.de/products/hibiscus)  

---

**Tools**

Bildschirmfotos: [Kazam](https://launchpad.net/kazam), [Flameshot](https://flameshot.js.org), [Grim](https://github.com/emersion/grim)  
ebook Reader: [Calibre](https://calibre-ebook.com)  
Make Startup Disk: [USB Sticks mit .iso Bootimage erstellen](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-ubuntu)  
Passwort-Manager: [KeePassXC](https://keepassxc.org)  
Nextclound Client: [Nextcloud](https://nextcloud.com/clients)  
Remote Desktop: [Remmina](https://remmina.org)  
Virtuelle Maschine: [Virtualbox](https://virtualbox.org)  
Verschlüsselung: [Veracrypt](https://veracrypt.fr)  
Desktop Automation Utility: [Autokey](https://github.com/autokey/autokey)  
Textvergleich: [Meld](https://meldmerge.org)  

---

**Kommunikation**

Email Client: [Thunderbird](https://thunderbird.net)  
XMPP Messenger Client: [Gajim](https://gajim.org/Gajim), [Dino](https://dino.im)  
Matrix Client: [Riot](https://riot.im)  
Voice Chat: [Mumble](https://mumble.info)  

---

**Web:**

FTP: [FilleZilla](https://filezilla-project.org/download.php)  
Browser: [Firefox](https://mozilla.org/en-US/firefox/)  

---

**Video & Audio**


Videoplayer: [VLC](https://videolan.org),[SmPlayer](https://smplayer.info)  
Audioplayer: [VLC](https://videolan.org), [Banshee](https://banshee.fm), [Amarok](https://amarok.kde.org)  
Audiokonverter: [Asunder](https://https//wiki.ubuntuusers.de/Asunder)  
Videoaufzeichnung/Live Streaming: [OBS Studio](https://obsproject.com), [SimpleScreenRecorder](https://maartenbaert.be/simplescreenrecorder)  
Videobearbeitung: [Openshot](https://openshot.org), [KdeLive](https://kdenlive.org)  
Audiobearbeitung: [Audacity](https://audacityteam.org)  

---

**Backups**


Systembackup: [Timeshift](https://github.com/teejee2008/timeshift)  
Komplette Backups: [Clonezilla](https://clonezilla.org)  
Backuplösung: [Rsync](https://wiki.ubuntuusers.de/rsync), [Luckybackup](https://luckybackup.sourceforge.net), [Restic](https://github.com/restic)  
File Synchronization: [Syncthing](https://syncthing.net)  

---


**Softwareentwickler:**


IDE: [Intellji](https://jetbrains.com/intellji), [Eclipse (Java)](https://eclipse.org), [Pycharm (Python)](https://jetbrains.com/pycharm), [Atom](https://github.com/atom), [Visual Studio Code](https://code.visualstudio.com/), [Emacs](https://gnu.org/s/emacs)  
Markdown Editor: [TYPORA](https://typora.io)  


**Sie brauchen weitere Ideen?**

[Arch Linux Wiki](https://wiki.archlinux.org/index.php/list_of_applications)
[Ubuntuusers](https://ubuntuusers.de/)  

---

### Smartphone (Android Only) ###

Wir erachten iOS nicht als datenschutzfreundlich im Sinne unserer Grundsätze. Aus diesem Grund gibt es keine gesonderte Auflistung für iOS Geräte.

---
 

**Sicherheit**

Firewall: [AFwall](https://kuketz-blog.de/f-droid-und-afwall-android-ohne-google-teil4/), [netguard](https://f-droid.org/en/packages/eu.faircode.netguard/)  
Trackerschutz: [AF-wall-Skripte](https://kuketz-blog.de/afwall-digitaler-tuervorsteher-take-back-control-teil4/),
[Blokada](https://f-droid.org/en/packages/org.blokada.alarm/)  

---

**Büro**

Dateimanager: [Amaze](https://f-droid.org/en/packages/com.amaze.filemanager)  
E-Mail: [K9 Mail](https://f-droid.org/packages/com.fsck.k9/), [FairEmail](https://f-droid.org/en/packages/eu.faircode.email/)  
Browser: [FOSS Browser](https://f-droid.org/en/packages/de.baumann.browser/),  
Messenger: [Conversations](https://f-droid.org/packages/eu.siacs.conversations/), [Pix-Art-Messenger](https://f-droid.org/en/packages/de.pixart.messenger)  
Kalender: [SimpleCalender](https://f-droid.org/en/packages/com.simplemobiletools.calendar.pro)
Mumble: [Plumble](https://f-droid.org/en/packages/com.morlunk.mumbleclient)  
VPN: [Wireguard](https://f-droid.org/en/packages/com.wireguard.android/), [OpenVPN](https://f-droid.org/packages/de.blinkt.openvpn/)  
PDF: [muPDF](https://f-droid.org/packages/com.artifex.mupdf.viewer.app/)  

---

**Media**

Kamera: [OpenCamera](https://f-droid.org/packages/net.sourceforge.opencamera/)  
Music: [VanillaMusic](https://f-droid.org/en/packages/ch.blinkenlights.android.vanilla/)  

---

**Unterwegs**

Zugverbindungen weltweit: [Transportr](https://f-droid.org/packages/de.grobox.liberario/)  
Reisekosten managen: [Tricky Tripper](https://f-droid.org/en/packages/de.koelle.christian.trickytripper/)  
Alternative zu Google Maps: [OsmAnd](https://f-droid.org/en/packages/net.osmand.plus)  

---

**Systemoptimierung**

Akkuoptimierung: [Battery Charge Limit](https://f-droid.org/en/packages/com.slash.batterychargelimit/)  
Tastatur: [Anysoftkey](https://search.f-droid.org/?q=Anysoftkey&lang=en)  

---
